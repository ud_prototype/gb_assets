/**
 * @version: v.1.1 -  TIAA-CREF Widgets for prototype
 * @date: 07-01-2013 
 * @copyright: Copyright (c) 2013, TIAA-CREF. All rights reserved.
 * @author: Prototype Team
 * @website: http://www.tiaa-cref.org
 */
 
(function($){

	// START --> Utilities	
	if(typeof Object.create !== 'function'){ 	//polyfill for object.create
		Object.create = function (obj){function F(){}; F.prototype = obj; return new F();};
	};
	
	var alertFallback = true;  			// console and alerts
	if (typeof console === "undefined" || typeof console.log === "undefined") {
		console = {};if (alertFallback) {console.log = function(msg) {alert(msg)};} else {console.log = function() {};}
	};
	
	$.fn.getIndex = function(){ 			// Get Index Utility
		var $sf = $(this), $p = $sf.parent().children(); return $p.index($sf);
	};
	// END --> Utilities
	
	// Carousel
	/**
	 * 	@requires jQuery 1.7
	 * 
	 *  Creates a new Carousel from an unordered list of elements
	 *  
	 *  @param list unordered list element
	 *  @param options options for the Carousel
	 *  @param options.animate (optional, default is true) true if sliding behavior is expected
	 *  @param options.autoRotate (optional, default is true) true if want to rotate elements automagically
	 *  @param options.autoRotateDelay (optional, default is 3s) delay in seconds between element rotation
	 *  @param options.speed (optional, default is 400 ms) speed for the sliding animation in milliseconds
	 */
	function Carousel(list, options){
		
		this.list = $(list);
		if(this.list.find(">li").length < 2){
			var self = this;
			setTimeout(function(){
				delete self;
			}, 200);
			return;
		}
		
		if(this.list.data("carousel")){
			this.list.data("carousel")._destroy();
		}
		this.list.data("carousel", this);
		
		this.settings = $.extend({},{
			animate: true,
			autoRotate: true,
			autoRotateDelay: 3,
			speed: 400
		}, options);
		
		this.action = this.settings.animate ? "animate" : "css";
		
		this._init();

		this.autoRotate = this.settings.autoRotate;
		if(this.autoRotate){
			this._startRotation();
		}
	}
	
	/**
	 *  Moves the carousel to passed page number
	 *  @param pageNumber number of the page to move to
	 */
	Carousel.prototype.goToPage = function(pageNumber){
		var pageNumber = parseInt(pageNumber, 10);
		if(isNaN(pageNumber)) return;
		this.list.css({
			left: -(this.contentWidth || this._getContentWidth()) * pageNumber
		});
		this._updatePlaylistSelection(pageNumber);
		this.loopIndex = pageNumber;
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._init = function(){
		var list = this.list;
		var settings = this.settings;
		
		var wrapper = list.closest(".carousel");
		var backBtn = wrapper.find(".back-button");
		var nextBtn = wrapper.find(".next-button");
		var items = list.find(">li");
		this.items = items;
		this.wrapper = wrapper;
		
		this.contentWidth = $(items[0]).width();
		if(this.contentWidth){
			this.list.width(items.length * this.contentWidth);
		}
		this.loopIndex = 0;
		this.noOfLoops = items.length;
		
		if(backBtn.length){
			backBtn.click($.proxy(this._goBack, this));
		}
		
		if(nextBtn.length){
			nextBtn.click($.proxy(this._goForward, this));
		}
		
		var self = this;
		var playListContainer = wrapper.find(".thumbsContainer");
		if(playListContainer.length){
			this.playList = playListContainer.find(">ul li");
			jQuery.each(this.playList, function(i, item){
				$(item).attr("data-index", i + "");
			});
			
			this.playList.click(function(e){
				e.preventDefault();
				self.goToPage($(this).attr('data-index'));
			});
		}
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._getContentWidth = function(){
		return $(this.items[0]).outerWidth();
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._updatePlaylistSelection = function(pageNumber){
		if(!this.playList) return;
		this.playList.removeClass("selected");
		$(this.playList[pageNumber]).addClass("selected");
	}
	
	/**
	 *	@private
	 */
	Carousel.prototype._startRotation = function(){
		var self = this;
		if(this.autoRotate){
			this.autoRotateTimer = setTimeout(function(){
				self._goForward();
			}, this.settings.autoRotateDelay * 1000);
		}
	};

	/**
	 *	@private
	 */
	Carousel.prototype._stopRotation = function(){
		if(this.autoRotateTimer) clearTimeout(this.autoRotateTimer);
		this.autoRotate = false;	
	};

	
	/**
	 *  @private
	 */
	Carousel.prototype._isLastPage = function(){
		 return (this.loopIndex == this.noOfLoops - 1);
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._isFirstPage = function(){
		return (this.loopIndex < 1);
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._goForward = function(e){
		if(e){
			e.preventDefault();
			this._stopRotation();
		}else{
			this._startRotation();	
		}
		var items = this.items;
		var list = this.list;
		if(this._isLastPage()){
			list.width((this.contentWidth || this._getContentWidth()) * (items.length + 1));
			$(items[0]).clone().appendTo(list);
			var that = this;
			this._updateListPos(-this.contentWidth * (this.loopIndex + 1), function(){	
				that.loopIndex = 0;
				list.css({"left": 0});
				list.find(">li").last().remove();
				list.width($(items[0]).width() * items.length);
				that._updatePlaylistSelection(that.loopIndex);
			});
		}else{
			this._updateListPos(-(this.contentWidth || this._getContentWidth()) * (++this.loopIndex));
			this._updatePlaylistSelection(this.loopIndex);
		}
		
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._goBack = function(e){
		if(e){
			e.preventDefault();
			this._stopRotation();
		}else{
			this._startRotation();
		}
		var that = this;
		var list = that.list;
		var items = that.items;
		if(this._isFirstPage()){
			list.width((this.contentWidth || this._getContentWidth()) * (items.length + 1));
			$(items[items.length - 1]).clone().prependTo(list);
			list.css({left: -that.contentWidth});
			var that = this;
			this._updateListPos(0, function(){	
				that.loopIndex = items.length - 1;
				list.find(">li").first().remove();
				list.width($(items[0]).width() * items.length);
				list.css({"left": -that.contentWidth * (items.length - 1)});
				that._updatePlaylistSelection(that.loopIndex);
			});
		}else{
			this._updateListPos(-(this.contentWidth || this._getContentWidth()) * (--this.loopIndex));
			this._updatePlaylistSelection(this.loopIndex);
		}

	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._updateListPos = function(pos, callback){
		this.list[this.action]({"left": pos}, this.settings.speed, callback || function(){});
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._destroy = function(){
		var list = this.list;
		if(this.autoRotateTimer){
			clearTimeout(autoRotateTimer);	
		}
		this.wrapper.find(".back-button, .next-button").unbind("click");
		var self = this;
		// IE7 needs some redirection when deleting "this"
		setTimeout(function(){
			delete self;
		}, 100);
	}
	
	
	// jQuery plugin
	$.fn.Carousel = function(options){
		
		return this.each(function(){
			new Carousel(this, options);
		});
	}
})(jQuery);


/// Removed older Tooltips

// ++++++++++++++++++++++++
// tip hover popup (supports tablet UI)

function getzIndex() {
		"use strict";
		// for performance, assumes highest z is on a div
		var t, z = 0;
		$('div').each(function() {
			t = Number($(this).css('zIndex'));
			z = t > z ? t : z;
		});
		return z;
	} // end fun


// initialize tooltips
	 
 $(document).ready(function() {
			// fix for dynamic tooltip content 	
			$(document).bind("dc-after-load", function(){

		bodyObj = $(document.body);
	
	if($('a.tipLink,a.tipLink2,a.infoLink,input.infoLink,input.tipLink,input.tipLink2').length > 0)
	  {
	  bodyObj.on('click','#tooltip',function(event){
	    event.preventDefault();
	    $('#tooltip').remove(); // tablet click-able close
	  });
	
	bodyObj.on('blur','input.infoLink,input.tipLink,input.tipLink2',function(event){
	  event.preventDefault();
	  $('#tooltip').remove(); // tablet click-able close
	});
	
	tooltip = function(){
            
            bodyObj.on('click mouseenter focus','a.infoLink,a.tipLink,a.tipLink2,input.infoLink,input.tipLink,input.tipLink2',function(event){
              event.preventDefault();
              var dataTipLocation,dataTipWidth,pointerObj,pointerHeight,pointerWidth,tipLink = $(this),timeoutVal,timeoutTrue,tipLinkHeight,tipLinkHeightHalf,tipLinkPos,tipLinkPosLeft,tipLinkPosTop,tipLinkTitle,tipLinkWidth,tipLinkWidthHalf,tooltip,tooltipClass,tooltipHeight,tooltipHeightHalf,tooltipTop,tooltipWidth,windowHeight,windowWidth,windowWidthHalf,windowObj,windowScrollTop,windowScrollLeft,posTop,posLeft;
              //tipLink.blur(); // tipLink = the tipLink/infoLink icon/text
              if(tipLink.css('visibility') === 'hidden') { return; }
            
              $('#tooltip').stop().remove();
              if($(tipLink.attr('href')).length > 0)
                {
                // create separate tag and assign anchor's href attribute as the id
                tipLinkTitle = $(tipLink.attr('href')).html(); // href="#text" -> id="text"
                }
              else if($('#'+tipLink.attr('aria-describedby')).length > 0)
                {
                // create separate tag and assign anchor's href attribute as the id
                tipLinkTitle = $('#'+tipLink.attr('aria-describedby')).html(); // aria-describedby="#text" -> id="text"
                }
              else
                {
                tipLinkTitle = tipLink.attr('title'); // set title in tipLink anchor
                }
              if(tipLinkTitle === '' || tipLinkTitle === undefined)
                {
                tipLinkTitle = tipLink.data('title');
                }
              if(tipLinkTitle === undefined) // final check
                {
                tipLinkTitle = 'See copydeck for help text.';
                }
              else // store data since preventing default tooltip clears source
                { tipLink.data('title',tipLinkTitle); }
              tipLink.attr('title',''); // prevent default tooltip
              bodyObj.append('<div id="tooltip"><a class="popup-close" href="#closeTip"><span class="popup-close-icon">Close</span></a><div class="pointer"></div><div class="bd">'+ tipLinkTitle +'</div></div>');
            
              // Determine viewport width and height and tooltip dimensions - used to keep tooltip from going outside viewport
              windowObj = $(window);
              windowHeight = windowObj.height();
              windowWidth = windowObj.width();
              windowWidthHalf = windowWidth/2;
              windowScrollTop = windowObj.scrollTop();
              windowScrollLeft = windowObj.scrollLeft();
              tooltip = $('#tooltip');
            
              // display the tooltip
              tooltip
                     .stop()
                     .css({'left':'-99999em','zIndex':getzIndex()+10,'maxWidth':windowWidthHalf+'px'})
                     .fadeIn();


      // Set width of tooltip
              dataTipWidth = Number(tipLink.attr('data-tooltip-width')); // markup override
              if(dataTipWidth !== undefined && dataTipWidth > 0) {
                        tooltipWidth = dataTipWidth;
              } else {
                        tooltipWidth = Math.min(530,tooltip.width()); // fit in window horizontally
              }
              tooltip.css('width',tooltipWidth + 'px'); // set width first to establish tooltip height

              // Get tipLink dimensions and position
              tipLinkPos = tipLink.offset();
              tipLinkPosLeft = tipLinkPos.left;
              tipLinkPosTop = tipLinkPos.top;
              tipLinkHeight = tipLink.height();
              tipLinkHeightHalf = tipLink.height()/2;
              tipLinkWidth = tipLink.width();
              tipLinkWidthHalf = tipLink.width()/2;
              // Get tooltip dimensions
              tooltipHeight = tooltip.height();
              tooltipHeightHalf = tooltipHeight/2;
              tooltipTop = tipLinkPosTop - tooltipHeightHalf + tipLinkHeightHalf;
              pointerObj = tooltip.find('div.pointer');

              dataTipLocation = tipLink.attr('data-tooltip-location'); // markup override
              if(dataTipLocation === undefined) { dataTipLocation = ''; }
              tooltipClass = 'infoHover visible ';

              // funcitons to calculate positons - saves time of updating each position if changes in future and only calculates when needed
              calcLeftPosition = function (location) {
                        if (location === 'vertical') {
                        posLeft = tipLinkPosLeft - (tooltipWidth/2 - tipLinkWidthHalf);
            } else if (location === 'left') {
                                    posLeft = tipLinkPosLeft - tooltipWidth - pointerWidth;
                        } else {
                                    posLeft = tipLinkPosLeft + tipLinkWidth;
                        }
              }
              calcTopPosition = function (location) {
                        if (location === 'bottom') {
                                    posTop = tipLinkPosTop + tipLinkHeight;
                        } else if (location === 'top') {
                                    posTop = tipLinkPosTop  - tooltipHeight - pointerHeight;
                        } else {
                        posTop = tipLinkPosTop - tooltipHeightHalf + tipLinkHeightHalf;
                        }
              }
              // Check Vertical position and adjust if needed
              checkVertical = function () {
                        // First check if tooltip is offscreen top
                        if(tooltipTop < windowScrollTop) {
                        // too far up
                        posTop = windowScrollTop - 1; // subtract 1 to hide top tooltip border in case part of pointer is cutoff
                        pointerObj.css('top',(tipLinkPosTop + tipLinkHeightHalf - windowScrollTop));
            }
                        // Next check if tooltip is offscreen bottom
            else if (tooltipHeight + tooltipTop > (windowHeight + windowScrollTop)) {
                        // too far down
                        posTop = windowHeight - tooltipHeight + windowScrollTop + 1; // add 1 to hide bottom tooltip border in case part of pointer is cutoff
                        pointerObj.css('top',(tipLinkPosTop + tooltipHeight - windowHeight - windowScrollTop + tipLinkHeightHalf));
            }
              }
              // Check Horizontal position and adjust if needed
              checkHorizontal = function () {
                        // First check if tooltip is offscreen right
            if ((posLeft + tooltipWidth) > (windowWidth + windowScrollLeft)) {
                        // too far right
                        posLeft = windowWidth + windowScrollLeft - tooltipWidth + 1; // add 1 to hide right tooltip border in case part of pointer is cutoff
                        pointerObj.css('left',(tipLinkPosLeft + tooltipWidth - windowWidth - windowScrollLeft + tipLinkWidthHalf));
            }
            // next check if tooltip is offscreen left
            else if (posLeft < windowScrollLeft) {
                        // too far left
                        posLeft = windowScrollLeft - 1; // subtract 1 to hide left tooltip border in case part of pointer is cutoff
                        pointerObj.css('left',Math.max((tipLinkPosLeft + tipLinkWidthHalf - pointerWidth/2 - windowScrollLeft),-pointerWidth/2)); // use max for tipLink2 to fix if way far left, pointer cutoff
            }
              }

              // when tabbing to tooltips, if tipLink is off-screen wait for scroll then show tooltip
              if ((tipLinkPosTop + tipLinkHeight) > (windowHeight + windowScrollTop) || (tipLinkPosTop + tipLinkHeight) < windowScrollTop || (tipLinkPosLeft + tipLinkWidth) < windowScrollLeft) {
                        // pause; page scrolls first, then tooltip vertically positions correctly
                        timeoutVal = 100;
                        timeoutTrue = true;
                } else { timeoutVal = 0; }
            
              t = setTimeout(function(){
                          if(timeoutTrue === true) {
                                    // recheck scroll distace if keyboard jumped to hidden tiplink
                                    windowScrollTop = windowObj.scrollTop();
                                    windowScrollLeft = windowObj.scrollLeft();
                          }
                          // Set Top and Bottom common positions and values
                          if (dataTipLocation === 'bottom' || dataTipLocation === 'top') {
                        // Set pointer dimensions
                                    pointerHeight = 10;
                                    pointerWidth = 19;
                                    
                            // Set left position to center on tiplink
                        calcLeftPosition('vertical');
                        // check horizontal position
                        checkHorizontal();
                          }
                          // Set Left and Right common positions and values
                          if (dataTipLocation === 'left' || dataTipLocation === 'right' || dataTipLocation === '') {
                        // Set pointer dimensions
                            pointerHeight = 32; // 25
                            pointerWidth = 10;

                            // Set top position
                        calcTopPosition('horizontal');
                        // check vertical position
                        checkVertical();
                          }
                          switch(dataTipLocation)
                            {
                            case 'bottom':
                                                // Set top position and pointer class
                                    if ((tooltipHeight + tipLinkPosTop + pointerHeight + tipLinkHeight) > (windowHeight + windowScrollTop)) {
                                                // too far down
                                                tooltipClass = tooltipClass + 'pointerBottom';
                                                calcTopPosition('top');
                                    } else {
                                                tooltipClass = tooltipClass + 'pointerTop';
                                                calcTopPosition('bottom');
                                    }
                                    break;
                            case 'left':
                                        // Set left position and pointer class
                                    if ((tipLinkPosLeft - tooltipWidth - pointerWidth - windowScrollLeft) < 0) {
                                                // too far left
                                                calcLeftPosition('right');
                                                tooltipClass = tooltipClass + 'pointerLeft';
                                    } else {
                                                calcLeftPosition('left');
                                                tooltipClass = tooltipClass + 'pointerRight';
                                    }
                                    break;
                            case 'top':
                                                // Set top position and pointer class
                                    if ((tipLinkPosTop - tooltipHeight - pointerHeight) < windowScrollTop) {
                                                // too far up
                                                tooltipClass = tooltipClass + 'pointerTop';
                                                calcTopPosition('bottom');
                                    } else {
                                                tooltipClass = tooltipClass + 'pointerBottom';
                                                calcTopPosition('top');
                                    }
                                    break;
                            default:
                                        // Set left position and pointer class
                                    if ((tipLinkPosLeft + tipLinkWidth + tooltipWidth + pointerWidth - windowScrollLeft) > windowWidth) {
                                                // too far right
                                                calcLeftPosition('left');
                                                tooltipClass = tooltipClass + 'pointerRight';
                                    } else {
                                                calcLeftPosition('right');
                                                tooltipClass = tooltipClass + 'pointerLeft';
                                    }
                                    break;
                  } // end switch location

                          // position the tooltip
                          tooltip.addClass(tooltipClass).css({
                                    top : posTop + 'px',
                                    left : posLeft + 'px'
                          });
              }, timeoutVal);
            });
            
            bodyObj.on('mouseleave','a.tipLink,a.tipLink2,a.infoLink,input.infoLink,input.tipLink,input.tipLink2',function(){
              var tId;
              // remove tip when not locked (hovered, allows links inside tip)
              tId = window.setTimeout(function(){ // allow time to hover tip
                if(!$('#tooltip').hasClass('tipLock')) { $('#tooltip').fadeOut(function(){$(this).remove();}); }
                }, 100);
            });
            
            bodyObj.on('mouseenter','#tooltip',function(){
              $('#tooltip').addClass('tipLock'); // lock tip in place while hovering
              }).on('mouseleave','#tooltip',function(){
                $('#tooltip').removeClass('tipLock').fadeOut(function(){$(this).remove();});
            }); // close mouseleave
            
            }; // close tooltip()
	
	tooltip(); // invoke tooltip
	
	bodyObj.on('click','a.tipLink,a.tipLink2,a.infoLink,input.infoLink,input.tipLink,input.tipLink2',function(event){
	  event.preventDefault();
	  $(this).blur();
	});
	} // end tip
	
	});
});

 // end initTooltips





// Action Link
;(function($){
	var ActionLink = function(ele,option){
		this.ele = ele;
		this.$ele = $(ele);
		this.$menu = $('#'+this.$ele.get(0).href.split('#')[1]);
		this.position = this.$ele.data('position');
		this.relative = this.$ele.data('relative');
		this.offsetEle = this.$ele.data('offset-element');
		this.offsetPx = this.$ele.data('offset-pixel');
		var that = this;
		this.menuVisible = function() {
			return that.$menu.is(':visible');
		};
		this.option = option;
	};

	ActionLink.prototype = {
		init: function() {
			var that = this,
				option = this.option;

			if(this[option]){
				this[option].call(this);
				return this;
			}
			
			this.$ele.off('.actionsMenu').on({
				'click.actionsMenu' : function(e){
					e.preventDefault();
					that.showMenu.call(that);
				},
				'mouseleave.actionsMenu' : function(){
					if (that.menuVisible()){
						that.hideMenu.call(that);
					}
				},
				'mouseenter.actionsMenu' : function(){
					if (that.menuVisible() && that.$ele.hasClass('active')){
						clearTimeout(ActionLink.menuTimeout);
					}
				},
				'keydown.actionsMenu' : function(e){
					var keys = {enter:13, esc:27, tab:9, left:37, up:38, right:39, down:40, spacebar:32};
					switch(e.keyCode) {
						case keys.enter:
							e.preventDefault();
							if (!that.menuVisible()){
								that.showMenu.call(that);
							}
							break;
						case keys.esc:
							if (that.menuVisible()){
								that.hideMenu.call(that,false);
							}
							break;
						case keys.tab:
							that.$ele.removeClass('active').trigger('actionLinkHide');
							that.hideMenu.call(that,false);
							break;
						case keys.down:
							e.preventDefault();
							if (that.menuVisible()){
								that.$menu.find('a:first').focus();
							} else {
								e.preventDefault();
								that.showMenu.call(that);
							}
							break;
						case keys.up:
							e.preventDefault();
							if (that.menuVisible()){
								that.hideMenu.call(that,false);
							}
							break;
						default:
							break;
					}
				},
				'focusin.actionsMenu' : $.proxy(this.showMenu, this)
			}).attr({
				'role' : 'button',
				'aria-haspopup' : 'true',
				'tabindex' : '0',
				'aria-owns' : that.$menu.attr('id')
			});
			this.$menu.attr({
				'aria-hidden' : 'true',
				'role' : 'menu',
				'aria-expanded' : 'false'
			}).find('li').attr('role', 'menuitem').find('a').attr('tabindex', -1);

			return this;
		},
		getPosition : function(){
			var leftPos = 0,
				$parentEle = this.$ele,
				offsetBy = 0;

			if (typeof this.offsetEle !== 'undefined'){
				$parentEle = $('#'+this.offsetEle);
			}

			var coords = $parentEle.offset();

			if (typeof this.relative !== 'undefined' && this.relative === true) {
				coords = $parentEle.position();
			}

			if (typeof this.offsetPx !== 'undefined'){
				offsetBy = this.offsetPx;
			}

			if(this.position === 'left'){
				leftPos = coords.left;
			} else {
				leftPos = coords.left + $parentEle.outerWidth() - this.$menu.outerWidth();
			}

			return {
				top : coords.top + $parentEle.outerHeight(),
				left : leftPos + offsetBy
			};
		},
		showMenu : function(){
			var that = this;
			var menuPos = this.getPosition();

			this.$ele.addClass('active').trigger('actionLinkShow');
			this.$menu.stop(true,true).css({
				top : menuPos.top,
				left : menuPos.left
			});

			if (this.menuVisible()) {
				clearTimeout(ActionLink.menuTimeout);
			} else {
				this.$menu.attr({
					'aria-hidden' : 'false',
					'aria-expanded' : 'true'
				}).fadeIn().off().on({
					'mouseenter.actionsMenu': function(){
						if (that.menuVisible()){
							clearTimeout(ActionLink.menuTimeout);
						}
					},
					'focusin.actionsMenu': function(){
						if (that.menuVisible()){
							clearTimeout(ActionLink.menuTimeout);
						}
					},
					'mouseleave.actionsMenu' : $.proxy(this.hideMenu, this)
				}).find('a').on({
					'keydown.actionsMenu' : function(e){
						var keys = {enter:13, esc:27, tab:9, left:37, up:38, right:39, down:40, spacebar:32},
							$this = $(this),
							$aCollection = that.$menu.find('a');
						switch(e.keyCode) {
							case keys.esc:
							case keys.tab:
								$this.blur();
								that.$ele.focus();
								that.hideMenu.call(that,false);
								break;
							case keys.down:
								e.preventDefault();
								$aCollection.each(function(i, a){
									if (i === $aCollection.length-1){
										return false;
									} else {
										if ($(a).is(':focus')){
											$this.blur();
											$aCollection.eq(i+1).focus();
											return false;
										}
									}
								});
								break;
							case keys.up:
								e.preventDefault();
								$aCollection.each(function(i, a){
									if ($(a).is(':focus')){
										if (i === 0){
											$this.blur();
											that.$ele.focus();
										} else {
											$this.blur();
											$aCollection.eq(i-1).focus();
											return false;
										}
									}
								});
								break;
							default:
								break;
						}
					},
					'click.actionsMenu' : $.proxy(this.hideMenu, this)
				});
			}
		},
		hideMenu : function(delay){
			var that = this;
			delay = (delay !== false) ? true : false;

			function hideThis(){
				that.$ele.removeClass('active').trigger('actionLinkHide');
				that.$menu.attr({
					'aria-hidden' : 'true',
					'aria-expanded' : 'false'
				}).stop(true,true).fadeOut('fast').find('a').off('.actionsMenu');
			}
			if (that.menuVisible() && delay){
				ActionLink.menuTimeout = setTimeout(function(){
					hideThis();
				}, 200);
			} else if (that.menuVisible() && !delay){
				hideThis();
			}
		},
		destroy : function(){
			this.$ele.off('.actionsMenu');
			this.$menu.find('li').removeAttr('role').find('a').off('.actionsMenu').attr('tabindex', 0);
		}
	};

	$.fn.actionLink = function(option) {
		return this.each(function() {
			new ActionLink(this, option).init();
		});
	};
}(jQuery));




var TIAA = {};
TIAA.ui = (function($){
	
	/* TODO: "jQuery.browser" is deprecated, find another way
	   to achieve this functionality (Do we need this?) */
	blockStyle = jQuery.browser.msie ? "block" : "table-row";
	
	var SELECTORS = {
		"CURRENT_YEAR": "span.currentYYYY",
		"LAST_YEAR": "span.lastYYYY",
		"INPUT_DATE": "input.inputDate",
		"ACCORDION_HEADER": ".panels .hd",
		"ACCORDION_CONTENT": ".panels .content",
		"FILTERS": ".filters li a, .noticeModule .cancel, .infoModule .cancel",
		"FILTERS_LIST": ".filters",
		"FILTERS_CLEAR_ALL": ".breadBox .clearLink",
		"GLOBAL_MSG_CONTAINER": ".noticeModule",
		"GLOBAL_MSG_CLOSE_LINK": ".globalmsg .noticeModule a.expanded",
		"GLOBAL_ALERT_NOTICE": ".pageheader .alertNotice",
		"GLOBAL_ALERT_NOTICE_OPEN_LINK": ".pageheader .alertNotice a.collapsed",
		"SWITCH_VIEW": ".viewSwitch li, .ynSwitch li",
		"EXPAND_COLLAPSE_LINK": ".jsExpandCollapse",
		"EXPAND_COLLAPSE_HEADER_ROW": "tr",
		"EXPAND_COLLAPSE_DETAIL_ROW": "tr.detail",
		"MODAL_POPUP": ".jq-modal",
		"LOADER_MODAL": ".jq-loader"
	};
	
	var CLASSES = {
		"ACCORDION_HEADER_EXPANDED": "hdexpanded",
		"ACCORDION_CONTENT_EXPANDED": "bgexpanded",
		"ACCORDION_ICON_EXPANDED": "expanded",
		"ACCORDION_ICON_COLLAPSED": "collapsed",
		"SWITCH_VIEW_SELECTED": "selected",
		"EXPAND_COLLAPSE_EXPANDED": "expanded",
		"EXPAND_COLLAPSE_COLLAPSED": "collapsed",
		"EXPAND_COLLAPSE_TOP_BACKGROUND": "bgExpandedTop",
		"EXPAND_COLLAPSE_BOTTOM_BACKGROUND": "bgExpandedBottom"
		
	};
	
	var EVENTS = {
		"CLICK": "click"	
	};
	
	/**
	 *  Updates current year for the span element with "currentYYYY" class
	 *  Updates previous year for the span element with "lastYYYY" class
	 *  Note: might be a good idea to separate these into 2 different functions
	 */
	function initCopyrightDates(){
		$(SELECTORS.CURRENT_YEAR).html(new Date().getFullYear() + "");
		$(SELECTORS.LAST_YEAR).html((new Date().getFullYear() - 1) + "");
	}
	
	/**
	 *  Initializes datepicker widget for all input elements with "inputDate" class
	 *  @param opts jQuery UI datepicker widget options  
	 */
	function initDatePicker(opts, container){
		var options = $.extend({},{
			constrainInput: true,
			min: '+1'
		}, opts);
		//var perpend = (!container ? "div:not(.popup) + " : "");
		var prepend = "";
		$(prepend+SELECTORS.INPUT_DATE, container || document).each(function(){var $this = $(this), x = $.extend({}, {yearRange: ($this.attr('data-yearRange'))?(($this.attr('data-yearRange').split(':')[1]=='current')?(($this.attr('data-yearRange').split(':')[0])+':'+(new Date().getFullYear())):($this.attr('data-yearRange'))):("c-10:c+10"), maxDate: ($this.attr('data-maxDate'))?($this.attr('data-maxDate')):(null), changeMonth: ($this.attr('data-changeMonth') == 'true' ? true : false), changeYear: ($this.attr('data-changeYear') == 'true' ? true : false)}, options); $this.datepicker(x);})
		$(prepend+".input-calendar", container || document).each(function(){var $this = $(this), x = $.extend({}, {yearRange: ($this.attr('data-yearRange'))?(($this.attr('data-yearRange').split(':')[1]=='current')?(($this.attr('data-yearRange').split(':')[0])+':'+(new Date().getFullYear())):($this.attr('data-yearRange'))):("c-10:c+10"), maxDate: ($this.attr('data-maxDate'))?($this.attr('data-maxDate')):(null), changeMonth: ($this.attr('data-changeMonth') == 'true' ? true : false), changeYear: ($this.attr('data-changeYear') == 'true' ? true : false)}, options); $this.datepicker(x);})
	}
	
	/**
	 *  Initializes expand/collapse behavior for the accordion panels.
	 *  Use this function to bind expand/collapse behavior with the header of the panel.
	 */
	function initAccordionWithHeader(){
	var $pn = $('div.panels');
	$pn.each(function(e){
		var className = $(this).attr('class'),
		$hdr = $(this).find($(SELECTORS.ACCORDION_HEADER)),
		$anc = $hdr.find('a'),
		$cntnt = $(this).find($(SELECTORS.ACCORDION_CONTENT));

		if(className.indexOf('acco') !== -1 && className.indexOf('panels') !== -1 ){	
			$hdr.find('a').attr('class',CLASSES.ACCORDION_ICON_COLLAPSED);
			$cntnt.addClass(CLASSES.ACCORDION_CONTENT_EXPANDED).hide().eq(0).show();
			$hdr.eq(0).addClass('hdexpanded');
			$anc.eq(0).attr('class',CLASSES.ACCORDION_ICON_EXPANDED);

			//Added for accordion
			if($('div.jq-accordions').length){
				var $self = $(this);			
				$(this).accordion({header:'div.hd',autoHeight: false,heightStyle: "content",
					create: function(eve, ui){
						var mdata = $(this).data('chart');
						if(mdata){
							var startNewChart = new StartNewChart (this, eve, mdata);
							startNewChart.init();
						};
					},
					/* Added event activate for newer version of jQuery UI */
					activate: function(eve, ui){			
					     	var $this= $(this), mdata = $this.data('chart'), i = $this.find('div.hd').index(ui.newHeader[0]);
					     	if(mdata){
							var startNewChart = new StartNewChart (ui, eve, mdata, i);
							startNewChart.init();
						};			     	
					}, 
					change: function(eve, ui){			
					     	var $this= $(this), mdata = $this.data('chart'), i = $this.find('div.hd').index(ui.newHeader[0]);
					     	if(mdata){
							var startNewChart = new StartNewChart (ui, eve, mdata, i);
							startNewChart.init();
						};			     	
					}
				}).find($hdr).click(function(){
					$self.find($(SELECTORS.ACCORDION_HEADER)).removeClass(CLASSES.ACCORDION_HEADER_EXPANDED).find('h3>a').attr('class',CLASSES.ACCORDION_ICON_COLLAPSED); 					
					$(this).addClass(CLASSES.ACCORDION_HEADER_EXPANDED).find('h3>a').attr('class',CLASSES.ACCORDION_ICON_EXPANDED);
				});
			};		
		}else{			
		// Expand Collapse Panel
			$(this).find('div.content').hide();
			$.each($anc, function(){
				if($(this).attr('class')=== 'expanded'){
					$(this).closest('div.hd').addClass(CLASSES.ACCORDION_HEADER_EXPANDED);
					$(this).closest('div.hd').next().addClass(CLASSES.ACCORDION_CONTENT_EXPANDED).slideDown();
				};
			});
			// $hdr.toggle(function() {
			// 	$(this).addClass(CLASSES.ACCORDION_HEADER_EXPANDED).next().addClass(CLASSES.ACCORDION_CONTENT_EXPANDED).slideDown();
			// 	$(this).find('>h3>a').attr('class',CLASSES.ACCORDION_ICON_EXPANDED);
					
			// },function(){	
			// 	$(this).removeClass(CLASSES.ACCORDION_HEADER_EXPANDED).next().removeClass(CLASSES.ACCORDION_CONTENT_EXPANDED).slideUp();
			// 	$(this).find('>h3>a').attr('class',CLASSES.ACCORDION_ICON_COLLAPSED);
			// });
			// $(this).find('a.expanded').eq(0).trigger('click');

			$hdr.off('click').on('click', function(){ var sf = $(this);
				if(sf.hasClass(CLASSES.ACCORDION_HEADER_EXPANDED)){
					sf.removeClass(CLASSES.ACCORDION_HEADER_EXPANDED).next().slideUp(function(){
						$(this).removeClass(CLASSES.ACCORDION_CONTENT_EXPANDED)
					});
					sf.find('>h3>a').attr('class',CLASSES.ACCORDION_ICON_COLLAPSED);
				}else{
					sf.addClass(CLASSES.ACCORDION_HEADER_EXPANDED).next().addClass(CLASSES.ACCORDION_CONTENT_EXPANDED).slideDown();
					sf.find('>h3>a').attr('class',CLASSES.ACCORDION_ICON_EXPANDED);
				};
				return false; 
			});
 		};
 	});	
};	
	
	/**
	 *  Initializes filter close behavior
	 */
	function initFilters(){
		//$(SELECTORS.FILTERS).live(EVENTS.CLICK, function(e){
//			e.preventDefault();
//			$(this).parent().fadeTo('fast', 0, function() {
//				$(this).slideUp('fast');
//			});
//		});
         $(SELECTORS.FILTERS).live(EVENTS.CLICK, function(e){
            e.preventDefault();
            $(this).parent().fadeTo('fast', 0, function() {
            $(this).slideUp('fast').addClass('closed'); // add closed class to easily get length of list
            	if($(this).parent().children('li:not(".closed")').length == 0){ // hide section when all list items are closed
				$(this).parent().parent().parent().slideUp('fast', function(){ 
                	$(this).removeClass('visible'); // remove visible class from notifications module
                });
              }
           });
        });
		$(SELECTORS.FILTERS_CLEAR_ALL).live(EVENTS.CLICK, function(e){
			e.preventDefault();
			$(this).parent().parent().fadeTo('fast', 0, function() {
				$(this).slideUp('fast');
			});
		});
	}
	
	/**
	 *  Initializes global alert close and open behavior
	 */
	function initGlobalAlert(){
		
		$(SELECTORS.GLOBAL_MSG_CLOSE_LINK).live(EVENTS.CLICK, function(e) {
			e.preventDefault();
			$(this).blur().closest(SELECTORS.GLOBAL_MSG_CONTAINER).slideUp('fast',function(){
				$(SELECTORS.GLOBAL_ALERT_NOTICE).slideDown('fast');
			});
		});
		
		$(SELECTORS.GLOBAL_ALERT_NOTICE_OPEN_LINK).live(EVENTS.CLICK, function(e) {
			e.preventDefault();
			var ele = $(this);
			ele.blur();
			$(SELECTORS.GLOBAL_MSG_CONTAINER).slideDown('fast');
			ele.closest(SELECTORS.GLOBAL_ALERT_NOTICE).slideUp('fast');
		});
	}
	
	
	/**
	 *  Initializes switch buttons (Including custom and yes/no)
	 */
	function initSwitchView(){
		$(SELECTORS.SWITCH_VIEW).live(EVENTS.CLICK, function(e){
			e.preventDefault();
			$(this).parent().find("li a").removeClass(CLASSES.SWITCH_VIEW_SELECTED)
							.end().end().children().first().addClass(CLASSES.SWITCH_VIEW_SELECTED);
		});
	}
	
	/**
	 *  Initialize expand/collapse behavior for anchor element with class "jsExpandCollapse"
	 *  Note: prototype pages are currently using inline call to "showHideTRs" function,
	 *  use this new implementation instead
	 */
	function initExpandCollapse(){
		$(SELECTORS.EXPAND_COLLAPSE_LINK).live(EVENTS.CLICK, function(e){
			e.preventDefault();
			var ele = $(this);
			ele.toggleClass(CLASSES.EXPAND_COLLAPSE_EXPANDED + " " + CLASSES.EXPAND_COLLAPSE_COLLAPSED);
			var headerRow = ele.closest(SELECTORS.EXPAND_COLLAPSE_HEADER_ROW);
			var detailRow = $(ele.attr("href"));
			detailRow.toggle("fast", function(){
				detailRow.find('td').first().toggleClass(CLASSES.EXPAND_COLLAPSE_BOTTOM_BACKGROUND);
				headerRow.find('td').toggleClass(CLASSES.EXPAND_COLLAPSE_TOP_BACKGROUND);
			});
			
		});
	}
	
	
	function initSimpleExpandCollapse(){
		$(".jq-expand-collapse").live(EVENTS.CLICK, function(e){
			e.preventDefault();
			var ele = $(this);
			var tgtEle = $(ele.attr("href"));
			if(ele.hasClass("collapsed")){
				ele.removeClass("collapsed").addClass("expanded");
				tgtEle.slideDown();
			}else{
				ele.removeClass("expanded").addClass("collapsed");
				tgtEle.slideUp();
			}
		});
	}
	
	function setTab(link, event) {
	if(event) {
		event.preventDefault();
	}

	var ulobj = $(link).closest('ul');

	$(ulobj).find('a').removeClass('selected');
	$(link).addClass('selected').blur();

	var duration = ulobj.hasClass('fading') ? 'slow' : 0;
	$('a', ulobj).each(function(i, item) {
		var tabobj = $(item).attr('rel');
		$(tabobj).hide();
	});
	$($(link).attr('rel')).fadeTo(duration, 1);
	}

		// Handle tab selection
	$('ul.tabs > li > a').live('click', function(e) {
		setTab(this, e);
	
		//Get Location of tab's content
		var contentLocation = $(this).attr('href');
	
		//Let go if not a hashed one
		if(contentLocation){

			if(contentLocation.charAt(0)=="#") {
	
				e.preventDefault();
	
				//Make Tab Active
				$(this).parent().siblings().children('a').removeClass('selected');
				$(this).addClass('selected');
	
				//Show Tab Content & add active class
				$(contentLocation).show().addClass('selected').siblings('.bd').hide().removeClass('selected');
			}
		}
	});
	
	function initTabs(){
		// Use jquery to hide all tab content initially in IE6
		if($.browser.msie && $.browser.version == 6){
			$('.tabs-container ul.tabs~div').css('display','none');
		}
		
		// Show tab content initially
		$('ul.tabs').each(function(){
			var initialContentLocation = $(this).find('li a.selected').attr('href');
			var initialContentLocationRel = $(this).find('li a.selected').attr('rel');
			if(initialContentLocation){
				if(initialContentLocation.charAt(0)=="#") {
					$(initialContentLocation).show().siblings('.bd').hide();
				} else if(initialContentLocationRel.charAt(0)=="#") {
					$(initialContentLocationRel).show().siblings('.bd').hide();
				}
			}
		});
		


	}
	// End tab functionality
		
	/**
	 *  Initializes modal popup open/close behavior for all modals.
	 *  The markup should have a class "widget-modal" and href attribute
	 *  should point to the id (#id) of the content div
	 */


function configurePopup() {
// configure popup with standard classes
//$('div.ui-dialog').bgiframe(); // hide select form element in IE6
//$('div.ui-dialog-titlebar').addClass('head');
$('div.ui-dialog-titlebar').addClass('head').attr('tabindex',-1);
$('span.ui-dialog-title').addClass('popup-title');
$('a.ui-dialog-titlebar-close').addClass('popup-close');
$('div.ui-draggable').addClass('popup-draggable');
$('span.ui-icon').addClass('popup-close-icon');
if($('div.ui-dialog:visible').hasClass('modal'))
  {
  $('div.ui-widget-overlay').addClass('modal-overlay');
  }
} // end fun

// had to move event handler outside of the createPopup function so that it's static.
// it's used for unbinding the event without unbinding all other click events.
var poplogic = function(event) {

if(event.type !== 'keypress' || event.which === 13) // allow tabbing
  {

  var thisObj = $(this);
  if(thisObj.is('a')) { event.preventDefault(); } // prevent anchor behavior
  thisObj.blur();

  var mypopup = thisObj.data('popup');
  if (mypopup.filter(SELECTORS.LOADER_MODAL).length) {
    var counter = 0;
    var self = $(this);
    var total = parseInt(mypopup.attr('data-total')) || 5;
    mypopup.find('.current').html('0');
    mypopup.find('.total').html(total);
    var timer;
    mypopup.find('.progress').progressbar({value: 0});
    var timerHandler = function() {
      if (counter/total < 1) {
        counter++;
        mypopup.find('.progress').progressbar({value: ((counter/total) * 100)});
        mypopup.find('.current').html(counter);
        mypopup.trigger('loader-step', [counter, total]);
      } else {
        mypopup.find('.progress').progressbar({value: 100});
        mypopup.find('.current').html(counter);
        clearInterval(timer);
        mypopup.trigger('loader-complete');
        var has_loader_event = false;
        for (var event in mypopup.data('events')) {
        	if (event == 'loader-complete') {
        		has_loader_event = true;
        		break;
        	}
        };
        if (!has_loader_event) {
            window.location.href = self.attr('href');
        }
        mypopup.dialog('close');
      }
    };
    timer = setInterval(timerHandler, parseInt(mypopup.attr('data-frequency')) || 250);
    mypopup.bind('dialogclose', function(event, ui){
        clearInterval(timer);
    });

  }

  mypopup.data('openerObj',thisObj); // track for accessibility: return focus on close
  mypopup.dialog('open');
  } // end accessibility
};

function createPopup(popupElements) {
"use strict";

/* Added to check for boolean options passed as strings that jQuery Dialog requires to be boolean */

function isStringBool(str){
var test;
	if(str.toLowerCase()==='false'){
		test = false;
	}
	else if (str.toLowerCase()==='true')
	{
		test = true;
	} 
	else
		{ // a boolean was not passed, return value as is 
			test = str;
	}
	return test;
}

popupElements.each(function(){
  var popup,popupCustomOptions,popupData,popupDefaultOptions,popupOptions,popupField,popupId;

  popup = $(this);
  popupId = popup.attr('id');

  // specify default popup dialog options
  popupDefaultOptions = {
          autoOpen:false,
          close:function(){
	    $(this).trigger('dc-close-modal');
            //if(popup.data('openerObj') !== '')
             // { popup.data('openerObj').focus(); } // return focus to opening button
            },
          closeOnEscape:true,
          closeText:'Close',
          dialogClass:'modal', // standard class
          draggable:false,
          hide:{effect:"fade",duration:600},
          minHeight:20,
          modal:true,
          open:function(){
            configurePopup();
            $(this).trigger('dc-load-modal');
            },
          resizable:false,
          show:{effect:"fade",duration:600},
          width:600 // not adjusted for css borders
          };

  // get optional custom popup options specified in data-popup attribute
  // format = data-popup="width:200,draggable:true"
  popupCustomOptions = {};
  if(popup.is('[data-popup]'))
    {
    popupData = popup.attr('data-popup').split(',');
    $.each(popupData,function(){
	    popupField = this.split(':');
      popupCustomOptions[popupField[0]] = isStringBool(popupField[1]);
    });
    }

  // combine default and custom options (custom values override default values)
  popupOptions = $.extend({},popupDefaultOptions,popupCustomOptions);

  // add values to adjust for css-defined borders
  //   makes width/height calculations unnecessary for developer
  popupOptions.width = parseInt(popupOptions.width,10) + 12;
  if(popupOptions.height)
    { popupOptions.height = parseInt(popupOptions.height,10) + 42; }

  // configure popup
  popup.dialog(popupOptions);
  // add event to anchor to open popup (href = popup '#'+id)
  // also unbinding previously bound popup events - this is so recurring calls to initialize popups don't result in multiple instances popping up
  $('a[href="#'+popupId+'"],a[data-loader="#'+popupId+'"],input[data-behavior="'+popupId+'"],button[data-behavior="'+popupId+'"]').data('popup', popup).unbind('click keypress', poplogic).on('click keypress', poplogic);

}); // end each popup

$('a.clickHelp,a.clickHelpLink').on('click',function(){
  if(event.type !== 'keypress' || event.which === 13)
    {
    event.preventDefault();
    var thisClick = $(this);
    thisClick.blur();
    } // end accessibility
  });

} // end create popup
//


	function initModalPopup(){

          if($('div.popup,form.popup').length > 0)
            {
            createPopup($('div.popup,form.popup'));
            }

// ++++++++++++++++++++++++
// generic, reusable close

/*
$('a.closePopup, a[href="#closePopup"], input.closePopup').on('click',function(event){
  event.preventDefault();
  var popupObj = $(this).closest('div[class~=popup]');
  if(popupObj.attr('id') === undefined)
    { popupObj = $(this).closest('form[class~=popup]'); }
  popupObj.dialog('close');
});
*/

$('a.closePopup,a[href="#closePopup"],input.closePopup,input[data-behavior~="closePopup"],button.closePopup,button[data-behavior~="closePopup"]').on('click keypress',function(event){
  if(event.type !== 'keypress' || event.which === 13)
    {
    if($(this).is('a')) { event.preventDefault(); } // prevent anchor behavior
    var popupObj = $(this).closest('div[class~=popup]');
    if(popupObj.attr('id') === undefined)
      { popupObj = $(this).closest('form[class~=popup]'); }
    popupObj.dialog('close');
    } // end accessibility
});

	};
	


	// For Expand / Collapsed All
	
	var initExpandAll = (function() {
		$(".jq-expand-all").click(function(e) {e.preventDefault();
			var ele = $(this);
			ele.parent().next().find('div.hd').each(function() {
				(ele.is('.addLink'))? 
					(!$(this).is('.hdexpanded') && $(this).trigger('click')):
					($(this).is('.hdexpanded') && $(this).trigger('click'));
			});
			ele.toggleClass('addLink minusLink');
		});
	});


	
// Remove Tooltips Initializes function


	/**
	 *  Initializes behavior for action links for elements with class "jq-actions"
	 */
	function initActionLinks(){
		$('.jq-actions').actionLink();
	}
	
	/** 
	 * Initializes  show more link behavior for element with wrapper div.more-less
	 * Will hide all li elements with index > 3 and toggle them when the show more link is clicked
	 */
	function initShowMoreLink(){
		$('.more-less').each(function(i, item) {
			var $navlist = $(item);			
			$('li:gt(2)', $navlist).hide();
			var link = $('<a href="#" class="toggle addLink"><span class="icon"></span>show more</a>');
			$navlist.append(link);
			$navlist.addClass('collapsed');
		});
		
		$('.more-less .toggle').click(function(event) {
			event.preventDefault();
			event.stopPropagation();
			var el = $(this);
			el.blur();

			var ml = $(this).closest('.more-less');
			ml.toggleClass('collapsed expanded');
					
			var linkText = 'show more';
			var iconClass = 'addLink';
			if (ml.hasClass('expanded')) {
				$('li', ml).show();
				linkText = 'show less';
				iconClass = 'minusLink';
			} else {
				$('li:gt(2)', ml).hide();
			}
			el.removeClass('addLink minusLink').addClass(iconClass);
			el.html("<span class='icon'></span>" + linkText);
		});
	}
	
// Combo Box

(function( $ ){

	var PLUGIN_NS = 'comboBox';
	var ComboBox = function ( target, options ) { 
		this.$SelectEle = $(target);
		this.truncate = $(target).data('truncate');
		this._init( target, options );
		return this; 

		this.options= $.extend(
			true, {
				DEBUG: false
			},
			options
		);
	}

	ComboBox.prototype._init = function ( target, options ) {
		var selectedTxt = this.$SelectEle.find('option:selected').text(),
			selectedVal = this.$SelectEle.find('option:selected').val(),
			selectedId = this.$SelectEle.attr('id');
			selectOptions = '',
			$label = $('label[for="'+selectedId+'"]'),
			that = this;

		if (typeof this.truncate == 'number'){
			selectedTxt = selectedTxt.length > this.truncate ? selectedTxt.substr(0, this.truncate) + '...' : selectedTxt;
		}

		$label.attr('id', this.$SelectEle.attr('id') + '-comboBox-label');

		this.$SelectEle.wrap('<div class="comboBox-container" />').after('<div class="comboBox-inner"><a role="button" href="#nogo" aria-haspopup="true" data-val=""><span class="icon"></span></a></div><div class="comboBox-menu"><ul role="listbox" aria-hidden="true"></ul></div>');

		this.$SelectEle.find('option').each(function(){
			var selected = $(this).is(':selected') ? ' class="active"' : ' ',
				ariaSelect = $(this).is(':selected') ? 'aria-selected="true"' : 'aria-selected="false"';
			selectOptions += '<li role="presentation"><a href="#nogo" role="option" tab-index="-1" data-val="'+$(this).val()+'" '+selected + ariaSelect+'>'+$(this).text()+'</a></li>'
		});

		this.$Menu = this.$SelectEle.siblings('.comboBox-menu').find('ul');
		this.$Target = this.$SelectEle.siblings('.comboBox-inner').find('a');

		this.$Target.append(selectedTxt).attr('data-val', selectedVal).attr('aria-labelledby', $label.attr('id'));
		this.$Menu.append(selectOptions);
		this.$ActiveItem = this.$SelectEle.next().find('ul li a.active');

		this._eventBindings();

		return this.$SelectEle;
	};

	ComboBox.prototype.toggleMenu = function() {
		if (this.$Menu.is(':visible')){
			this.hideMenu();
		} else {
			this.showMenu();
		}
	}

	ComboBox.prototype.showMenu = function() {
		this.$SelectEle.blur();
		this.$Menu.attr('aria-hidden', 'false').show().find('li a.active').focus();
		return this.$SelectEle;
	}

	ComboBox.prototype.hideMenu = function() {
		this.$Menu.attr('aria-hidden', 'true').hide();
		return this.$SelectEle;
	}

	ComboBox.prototype.selectItem = function(item) {
		var $item = $(item);
			textContent = $(item).text();
		
		if (typeof this.truncate == 'number'){
			textContent = textContent.length > this.truncate ? textContent.substr(0, this.truncate) + '...' : textContent;
		}

		this.$Menu.find('li a').removeClass('active').attr('aria-selected', 'false');
		this.$Target.attr('data-val', $item.attr('data-val')).html('<span class="icon"></span>'+textContent);
		$item.addClass('active').attr('aria-selected', 'true');

		this.$Menu.hide();
		this.$Target.focus();
		this.$SelectEle.val($item.attr('data-val'));
		return this.$SelectEle;
	}

	ComboBox.prototype._nextItem = function() {
		var $aCollection = this.$Menu.find('li a'),
			that = this;

		$aCollection.each(function(i, a){
			if (i === $aCollection.length-1){
				that.$ActiveItem = $(a);
				return false;
			} else {
				if ($(a).hasClass('active')){
					that.$ActiveItem = $aCollection.eq(i+1);
					return false;
				}
			}
		});
	}

	ComboBox.prototype._nextFocus = function() {
		var $aCollection = this.$Menu.find('li a'),
			that = this;

		$aCollection.each(function(i, a){
			if (i === $aCollection.length-1){
				that.$FocusItem = $(a);
				return false;
			} else {
				if($(a).is(':focus')){
					that.$FocusItem = $aCollection.eq(i+1);
					that.$FocusItem.focus();
					return false;
				}
			}
		});
	}

	ComboBox.prototype._prevItem = function() {
		var $aCollection = this.$Menu.find('li a'),
			that = this;

		$aCollection.each(function(i, a){
			if ($(a).hasClass('active') && i === 0){
				that.$ActiveItem = $(a);
				return false;
			} else {
				if ($(a).hasClass('active')){
					that.$ActiveItem = $aCollection.eq(i-1);
					return false;
				}
			}
		});
	}

	ComboBox.prototype._prevFocus = function() {
		var $aCollection = this.$Menu.find('li a'),
			that = this;

		$aCollection.each(function(i, a){
			if ($(a).is(':focus') && i === 0){
				that.$FocusItem = $(a);
				return false;
			} else {
				if ($(a).is(':focus')){
					that.$FocusItem = $aCollection.eq(i-1);
					that.$FocusItem.focus();
					return false;
				}
			}
		});
	}

	ComboBox.prototype._eventBindings = function() {
		var that = this;

		this.$Target.on({
			'click.comboBox' : function(e){
				e.preventDefault();
				that.toggleMenu();
			},
			'keydown.comboBox' : function(e){
				var keys = {enter:13, esc:27, tab:9, left:37, up:38, right:39, down:40, spacebar:32};
				switch(e.keyCode) {
					case keys.spacebar:
						e.preventDefault();
						that.showMenu();
						break;
					case keys.tab:
						that.hideMenu();
						break;
					case keys.enter:
						return false;
						break;
					case keys.down:
						e.preventDefault();
						that._nextItem();
						that.selectItem(that.$ActiveItem);
						break;
					case keys.up:
						e.preventDefault();
						that._prevItem();
						that.selectItem(that.$ActiveItem);
						break;
					default:
						break;
				}
			}
		});
		this.$Menu.find('li a').on({
			'click.comboBox' : function(e){
				e.preventDefault();
				that.selectItem(this);
			},
			'keydown.comboBox' : function(e){
				var keys = {enter:13, esc:27, tab:9, left:37, up:38, right:39, down:40, spacebar:32};
				switch(e.keyCode) {
					case keys.enter:
					case keys.spacebar:
						e.preventDefault();
						that.selectItem(that.$FocusItem);
						that.hideMenu();
						break;
					case keys.tab:
						that.hideMenu();
						break;
					case keys.esc:
						that.hideMenu();
						that.$Target.focus();
						return false;
						break;
					case keys.down:
						e.preventDefault();
						that._nextFocus();
						break;
					case keys.up:
						e.preventDefault();
						that._prevFocus();
						break;
					default:
						break;
				}
			}
		});

		$('body').on('click',function(e) {
			if(!$(e.target).parents().hasClass('comboBox-container')){ 
				that.hideMenu(); 
			}
		});
	}

	$.fn[ PLUGIN_NS ] = function( methodOrOptions ) {
		return this.each(function() {
			if (!$(this).length) {
				return $(this);
			}
			var instance = $(this).data(PLUGIN_NS);

			// CASE: action method (public method on PLUGIN class)        
			if ( instance 
					&& methodOrOptions.indexOf('_') != 0 
					&& instance[ methodOrOptions ] 
					&& typeof( instance[ methodOrOptions ] ) == 'function' ) {

				instance[ methodOrOptions ]( Array.prototype.slice.call( arguments, 1 ) );

			// CASE: argument is options object or empty = initialise            
			} else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {

				instance = new ComboBox( $(this), methodOrOptions );
				$(this).data( PLUGIN_NS, instance );
				return $(this);

			// CASE: method called before init
			} else if ( !instance ) {
				$.error( 'Plugin must be initialised before using method: ' + methodOrOptions );
			
			// CASE: invalid method
			} else if ( methodOrOptions.indexOf('_') == 0 ) {
				$.error( 'Method ' +  methodOrOptions + ' is private!' );
			} else {
				$.error( 'Method ' +  methodOrOptions + ' does not exist.' );
			}
		});
	};
})(jQuery);


var initComboBox = function(){
	$('.comboBox-source').comboBox();
}

// end combobox fun

	/**
	 *  Public API
	 */
	return {
		initCopyrightDates: initCopyrightDates,
		initDatePicker: initDatePicker,
		initAccordionWithHeader: initAccordionWithHeader,
		initFilters: initFilters,
		initGlobalAlert: initGlobalAlert,
		initSwitchView: initSwitchView,
		initExpandCollapse: initExpandCollapse,
		initModalPopup: initModalPopup,
		initSimpleExpandCollapse: initSimpleExpandCollapse,
		initTabs: initTabs,
		initExpandAll: initExpandAll,
		//initTooltips: initTooltips,
		initActionLinks: initActionLinks,
		initShowMoreLink: initShowMoreLink,
		initComboBox: initComboBox,
		initAll: function(){
			$(document).bind("dc-after-load", function(){
				initCopyrightDates();
				initDatePicker();
				initAccordionWithHeader();
				initFilters();
				initGlobalAlert();
				initSwitchView();
				initExpandCollapse();
				initSimpleExpandCollapse();
				initModalPopup();
				initTabs();
				initExpandAll();
				//initTooltips();
				initActionLinks();
				initShowMoreLink();
				initComboBox();
			});
		}
	}
	
})(jQuery);


// initialize
jQuery(document).ready(function($) {

	TIAA.ui.initAll();

});

//
//
// ****************
// shared functions

function showHide(obj) {
	if (obj.is(':hidden')) {
		obj.removeClass('hidden');
	} else {
		obj.addClass('hidden');
	}
}

/* NOTE: Why do we have the same function again,
   there is no function overloading in JavaScript.
   The interpreter is just going to use the
   last found function. Which one do we need?

function showHide(shObj) {
	$(shObj).slideToggle('fast');
	$(shObj).toggleClass('bgexpanded');
}*/

function dollarFormatter(v, axis) {
	v += '';
	x = v.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return '$' + x1 + x2;
}


// For features carousel on the right
$(document).bind('dc-after-load', function(){
	if ($('.features-slider ul').length) {
		$('.features-slider ul').each(function(){
			
			var ele = $(this);
			var items = ele.find(">li");
			
			//var height = items.first().outerHeight();	
//			ele.height(height);
			
			var headers = ele.find("h4");
			
			headers.css({
				position: 'absolute',
				top: 0,
				left: 0	
			}).width(ele.first().width() - (headers.first().outerWidth() - headers.first().width()) - 2);
			
			// create the pager markup
			var pagerHtml = [];
			pagerHtml.push("<div class='bx-pager'>");
			var len = items.length;

			for(var i = 0; i < len; i++){
				pagerHtml.push("<a href='javascript:void(0)' class='pager-link' data-index='");
				pagerHtml.push(i);
				pagerHtml.push("'>");
				pagerHtml.push(i + 1);
				pagerHtml.push("</a>");
			}

			pagerHtml.push("</div>");
			ele.parent().append(pagerHtml.join(""));

			var pagerItems = ele.parent().find('.bx-pager > a');
			
			pagerItems.click(function(e){
				e.preventDefault();
				goToNextItem(parseInt($(this).attr('data-index'), 10));
			});

			var curIndex = 0;

			items.hide();
			items.first().show();
			pagerItems.first().addClass("pager-active");
			
			var timerRef;	
	
			function goToNextItem(itemIndex){
				
				if(timerRef) clearTimeout(timerRef);
				
				$(items[curIndex]).fadeOut(1500);
				$(pagerItems[curIndex]).removeClass("pager-active");
				
				if(typeof(itemIndex) != 'undefined' && itemIndex >= 0){	
					curIndex = itemIndex;
				}else{
					curIndex = curIndex + 1;
				}
				
				if(curIndex === len) curIndex = 0;

				$(items[curIndex]).fadeIn(1500, function(){
					timerRef = setTimeout(function(){
						goToNextItem();
					}, 10000);
				});	
				$(pagerItems[curIndex]).addClass("pager-active");
				
			}
	
			timerRef = setTimeout(goToNextItem, 10000);	
				
		});	
	}	
	
});

/*
 * jQuery Plugin to obtain touch gestures
 * @author Andreas Waltl, netCU Internetagentur (http://www.netcu.de)
 */
(function($){$.fn.touchwipe=function(settings){var config={min_move_x:20,min_move_y:20,wipeLeft:function(){},wipeRight:function(){},wipeUp:function(){},wipeDown:function(){},preventDefaultEvents:true};if(settings)$.extend(config,settings);this.each(function(){var startX;var startY;var isMoving=false;function cancelTouch(){this.removeEventListener('touchmove',onTouchMove);startX=null;isMoving=false}function onTouchMove(e){if(config.preventDefaultEvents){e.preventDefault()}if(isMoving){var x=e.touches[0].pageX;var y=e.touches[0].pageY;var dx=startX-x;var dy=startY-y;if(Math.abs(dx)>=config.min_move_x){cancelTouch();if(dx>0){config.wipeLeft()}else{config.wipeRight()}}else if(Math.abs(dy)>=config.min_move_y){cancelTouch();if(dy>0){config.wipeDown()}else{config.wipeUp()}}}}function onTouchStart(e){if(e.touches.length==1){startX=e.touches[0].pageX;startY=e.touches[0].pageY;isMoving=true;this.addEventListener('touchmove',onTouchMove,false)}}if('ontouchstart'in document.documentElement){this.addEventListener('touchstart',onTouchStart,false)}});return this}})(jQuery);


(function($){	// START  <== Show more or less  
	var ShowMoreLess = {
		init: function(elem){
			var my = this; my.$elem = $(elem); my.more = my.$elem.attr('data-more') || 'Show More'; my.less = my.$elem.attr('data-less') || 'Show Less'; my.def = my.$elem.attr('data-default') || 'close'; my.mySpeed = my.$elem.attr('data-speed')*1000 || 400;
			my.flag = (my.def==="close") ? 0 : 1;
			my.temp = $.trim('<a class="tc-showmorelink {{cssState}}" title="{{title}}" href="#"><span class="icon"></span><span class="text"> {{link}}</span></a>');
			my.flagging();
		},
		flagging: function(){ var my = this;
			(my.flag === 0) ? (function(){my.anchClass = 'addLink';my.$elem.hide();my.act = my.more;})():
					(function(){my.anchClass = 'minusLink';my.$elem.show();my.act = my.less;})();
		},
		construct: function(){var my = this;
			my.anch = $(my.temp.replace(/{{cssState}}/ig, my.anchClass).replace(/{{title}}/ig, my.act).replace(/{{link}}/ig, my.act)).insertAfter(my.$elem).on('click', function(e){e.preventDefault(); my.clickFunc.call(my)});
		},
		clickFunc: function(){var my = this;
			my.$elem.slideToggle(my.mySpeed);
			if(my.anch.hasClass('minusLink')){
				my.anch.removeClass('minusLink').addClass('addLink').attr('title',my.more).find('span.text').text(my.more);
			}else{
				my.anch.removeClass('addLink').addClass('minusLink').attr('title',my.less).find('span.text').text(my.less);
			};
		}
	};
	$.fn.showMoreLess = function(){
		return this.each(function(i){
			 var showMR = Object.create(ShowMoreLess);
			 showMR.init(this); showMR.construct();
		});
	};
	$(function(){ 
		$('.tc-showmore').showMoreLess(); // Initiating show More/Less
	});
})(jQuery); 	// END  <== Show more or less 
