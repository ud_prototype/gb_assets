/**
 * @version: v.1.1 -  TIAA-CREF Widgets for prototype
 * @date: 04-17-2013 10:04
 * @copyright: Copyright (c) 2013, TIAA-CREF. All rights reserved.
 * @author: Prototype Team
 * @website: http://www.tiaa-cref.org
 */
(function($){

	// START --> Utilities	
	if(typeof Object.create !== 'function'){ 	//polyfill for object.create
		Object.create = function (obj){function F(){}; F.prototype = obj; return new F();};
	};
	
	var alertFallback = true;  			// console and alerts
	if (typeof console === "undefined" || typeof console.log === "undefined") {
		console = {};if (alertFallback) {console.log = function(msg) {alert(msg)};} else {console.log = function() {};}
	};
	
	$.fn.getIndex = function(){ 			// Get Index Utility
		var $sf = $(this), $p = $sf.parent().children(); return $p.index($sf);
	};
	// END --> Utilities
	
	// Carousel
	/**
	 * 	@requires jQuery 1.7
	 * 
	 *  Creates a new Carousel from an unordered list of elements
	 *  
	 *  @param list unordered list element
	 *  @param options options for the Carousel
	 *  @param options.animate (optional, default is true) true if sliding behavior is expected
	 *  @param options.autoRotate (optional, default is true) true if want to rotate elements automagically
	 *  @param options.autoRotateDelay (optional, default is 3s) delay in seconds between element rotation
	 *  @param options.speed (optional, default is 400 ms) speed for the sliding animation in milliseconds
	 */
	function Carousel(list, options){
		
		this.list = $(list);
		if(this.list.find(">li").length < 2){
			var self = this;
			setTimeout(function(){
				delete self;
			}, 200);
			return;
		}
		
		if(this.list.data("carousel")){
			this.list.data("carousel")._destroy();
		}
		this.list.data("carousel", this);
		
		this.settings = $.extend({},{
			animate: true,
			autoRotate: true,
			autoRotateDelay: 3,
			speed: 400
		}, options);
		
		this.action = this.settings.animate ? "animate" : "css";
		
		this._init();

		this.autoRotate = this.settings.autoRotate;
		if(this.autoRotate){
			this._startRotation();
		}
	}
	
	/**
	 *  Moves the carousel to passed page number
	 *  @param pageNumber number of the page to move to
	 */
	Carousel.prototype.goToPage = function(pageNumber){
		var pageNumber = parseInt(pageNumber, 10);
		if(isNaN(pageNumber)) return;
		this.list.css({
			left: -(this.contentWidth || this._getContentWidth()) * pageNumber
		});
		this._updatePlaylistSelection(pageNumber);
		this.loopIndex = pageNumber;
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._init = function(){
		var list = this.list;
		var settings = this.settings;
		
		var wrapper = list.closest(".carousel");
		var backBtn = wrapper.find(".back-button");
		var nextBtn = wrapper.find(".next-button");
		var items = list.find(">li");
		this.items = items;
		this.wrapper = wrapper;
		
		this.contentWidth = $(items[0]).width();
		if(this.contentWidth){
			this.list.width(items.length * this.contentWidth);
		}
		this.loopIndex = 0;
		this.noOfLoops = items.length;
		
		if(backBtn.length){
			backBtn.click($.proxy(this._goBack, this));
		}
		
		if(nextBtn.length){
			nextBtn.click($.proxy(this._goForward, this));
		}
		
		var self = this;
		var playListContainer = wrapper.find(".thumbsContainer");
		if(playListContainer.length){
			this.playList = playListContainer.find(">ul li");
			jQuery.each(this.playList, function(i, item){
				$(item).attr("data-index", i + "");
			});
			
			this.playList.click(function(e){
				e.preventDefault();
				self.goToPage($(this).attr('data-index'));
			});
		}
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._getContentWidth = function(){
		return $(this.items[0]).outerWidth();
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._updatePlaylistSelection = function(pageNumber){
		if(!this.playList) return;
		this.playList.removeClass("selected");
		$(this.playList[pageNumber]).addClass("selected");
	}
	
	/**
	 *	@private
	 */
	Carousel.prototype._startRotation = function(){
		var self = this;
		if(this.autoRotate){
			this.autoRotateTimer = setTimeout(function(){
				self._goForward();
			}, this.settings.autoRotateDelay * 1000);
		}
	};

	/**
	 *	@private
	 */
	Carousel.prototype._stopRotation = function(){
		if(this.autoRotateTimer) clearTimeout(this.autoRotateTimer);
		this.autoRotate = false;	
	};

	
	/**
	 *  @private
	 */
	Carousel.prototype._isLastPage = function(){
		 return (this.loopIndex == this.noOfLoops - 1);
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._isFirstPage = function(){
		return (this.loopIndex < 1);
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._goForward = function(e){
		if(e){
			e.preventDefault();
			this._stopRotation();
		}else{
			this._startRotation();	
		}
		var items = this.items;
		var list = this.list;
		if(this._isLastPage()){
			list.width((this.contentWidth || this._getContentWidth()) * (items.length + 1));
			$(items[0]).clone().appendTo(list);
			var that = this;
			this._updateListPos(-this.contentWidth * (this.loopIndex + 1), function(){	
				that.loopIndex = 0;
				list.css({"left": 0});
				list.find(">li").last().remove();
				list.width($(items[0]).width() * items.length);
				that._updatePlaylistSelection(that.loopIndex);
			});
		}else{
			this._updateListPos(-(this.contentWidth || this._getContentWidth()) * (++this.loopIndex));
			this._updatePlaylistSelection(this.loopIndex);
		}
		
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._goBack = function(e){
		if(e){
			e.preventDefault();
			this._stopRotation();
		}else{
			this._startRotation();
		}
		var that = this;
		var list = that.list;
		var items = that.items;
		if(this._isFirstPage()){
			list.width((this.contentWidth || this._getContentWidth()) * (items.length + 1));
			$(items[items.length - 1]).clone().prependTo(list);
			list.css({left: -that.contentWidth});
			var that = this;
			this._updateListPos(0, function(){	
				that.loopIndex = items.length - 1;
				list.find(">li").first().remove();
				list.width($(items[0]).width() * items.length);
				list.css({"left": -that.contentWidth * (items.length - 1)});
				that._updatePlaylistSelection(that.loopIndex);
			});
		}else{
			this._updateListPos(-(this.contentWidth || this._getContentWidth()) * (--this.loopIndex));
			this._updatePlaylistSelection(this.loopIndex);
		}

	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._updateListPos = function(pos, callback){
		this.list[this.action]({"left": pos}, this.settings.speed, callback || function(){});
	}
	
	/**
	 *  @private
	 */
	Carousel.prototype._destroy = function(){
		var list = this.list;
		if(this.autoRotateTimer){
			clearTimeout(autoRotateTimer);	
		}
		this.wrapper.find(".back-button, .next-button").unbind("click");
		var self = this;
		// IE7 needs some redirection when deleting "this"
		setTimeout(function(){
			delete self;
		}, 100);
	}
	
	
	// jQuery plugin
	$.fn.Carousel = function(options){
		
		return this.each(function(){
			new Carousel(this, options);
		});
	}
})(jQuery);


/// Removed older Tooltips

// ++++++++++++++++++++++++
// tip hover popup (supports tablet UI)

function getzIndex() {
		"use strict";
		// for performance, assumes highest z is on a div
		var t, z = 0;
		$('div').each(function() {
			t = Number($(this).css('zIndex'));
			z = t > z ? t : z;
		});
		return z;
	} // end fun


// initialize
$(document).ready(function() {

	bodyObj = $(document.body);

if($('a.tipLink,a.tipLink2,a.infoLink,input.infoLink,input.tipLink,input.tipLink2').length > 0)
  {
  bodyObj.on('click','#tooltip',function(event){
    event.preventDefault();
    $('#tooltip').remove(); // tablet click-able close
  });

bodyObj.on('blur','input.infoLink,input.tipLink,input.tipLink2',function(event){
  event.preventDefault();
  $('#tooltip').remove(); // tablet click-able close
});

tooltip = function(){

bodyObj.on('click mouseenter focus','a.infoLink,a.tipLink,a.tipLink2,input.infoLink,input.tipLink,input.tipLink2',function(event){
  event.preventDefault();
  var containerMidpoint,dataTipLocation,dataTipWidth,tipLinkLeftPos,leftRight,pointerObj,pointerHeight,pointerStop,t,tipLink = $(this),timeoutVal,tipLink2,tipLinkHeight,tipLinkPos,tipLinkTitle,tipLinkWidth,tooltip,tooltipClass,tooltipHeight,tooltipHeightHalf,tooltipTop,tooltipWidth,topOffset,topPos,windowHeight,windowObj;
  //tipLink.blur(); // tipLink = the tipLink/infoLink icon/text
  if(tipLink.css('visibility') === 'hidden') { return; }

  $('#tooltip').stop().remove();
  if($(tipLink.attr('href')).length > 0)
    {
    // create separate tag and assign anchor's href attribute as the id
    tipLinkTitle = $(tipLink.attr('href')).html(); // href="#text" -> id="text"
    }
  else if($('#'+tipLink.attr('aria-describedby')).length > 0)
    {
    // create separate tag and assign anchor's href attribute as the id
    tipLinkTitle = $('#'+tipLink.attr('aria-describedby')).html(); // aria-describedby="#text" -> id="text"
    }
  else
    {
    tipLinkTitle = tipLink.attr('title'); // set title in tipLink anchor
    }
  if(tipLinkTitle === '' || tipLinkTitle === undefined)
    {
    tipLinkTitle = tipLink.data('title');
    }
  if(tipLinkTitle === undefined) // final check
    {
    tipLinkTitle = 'See copydeck for help text.';
    }
  else // store data since preventing default tooltip clears source
    { tipLink.data('title',tipLinkTitle); }
  tipLink.attr('title',''); // prevent default tooltip
  bodyObj.append('<div id="tooltip"><a class="popup-close" href="#closeTip"><span class="popup-close-icon">Close</span></a><div class="pointer"></div><div class="bd">'+ tipLinkTitle +'</div></div>');

  tooltip = $('#tooltip');

  // position pop-up after open
  containerMidpoint = $('#container').width()/2; // for left/right tooltips
  //containerMidpoint = $('#container').width(); // for "fixed alignment" tooltips
  tipLink2 = tipLink.hasClass('tipLink2'); // test for alternate source

  tipLinkPos = tipLink.offset();
  tipLinkLeftPos = tipLinkPos.left;
  leftRight = 'left';
  tipLinkHeight = tipLink.height();
  if(tipLinkLeftPos > containerMidpoint) // pointer heights differ; store before tipLinkLeftPos adjustments
    {
    pointerHeight = 32; // 25
    pointerStop = -5;
    }
  else
    {
    pointerHeight = 20; // 19
    pointerStop = 1;
    }

  // display the tooltip
  tooltip
         .stop()
         .css({'left':'-99999em','zIndex':getzIndex()+10,'maxWidth':containerMidpoint+'px'})
         .fadeIn();
       //.bgiframe(); // hide select form element in IE6

  tooltipWidth = Math.min(530,tooltip.width()); // fit in window horizontally
  tipLinkWidth = tipLink.height(); // input tag width = 0; assumes square image //tipLinkWidth = tipLink.width();

  dataTipWidth = Number(tipLink.attr('data-tooltip-width')); // markup override
  if(dataTipWidth !== undefined && dataTipWidth > 0)
    { tooltipWidth = dataTipWidth; }
  tooltip.css('width',tooltipWidth + 'px'); // set width first to establish tooltip height

  dataTipLocation = tipLink.attr('data-tooltip-location'); // markup override
  if(dataTipLocation === undefined) { dataTipLocation = ''; }
  tooltipClass = 'infoHover visible ';
  switch(dataTipLocation)
    {
    case 'bottom':
      tipLinkLeftPos = tipLinkLeftPos - tooltipWidth/2 + tipLinkWidth + 5;
      tooltipClass = tooltipClass + 'pointerTop';
      topOffset = -(tipLinkHeight + 5);
      break;
    case 'left':
      tipLinkLeftPos = tipLinkLeftPos - tooltipWidth - 10;
      tooltipClass = tooltipClass + 'pointerRight';
      topOffset = tooltip.height()/2 - tipLinkHeight/2;
      break;
    case 'top':
      tipLinkLeftPos = tipLinkLeftPos - tooltipWidth/2 + tipLinkWidth + 5;
      tooltipClass = tooltipClass + 'pointerBottom';
      topOffset = tooltip.height() + tipLinkHeight;
      break;
    default:
      if(tipLinkLeftPos > containerMidpoint)
        {
        tipLinkLeftPos = tipLinkLeftPos - tooltipWidth - 10;
        tooltipClass = tooltipClass + 'pointerRight';
        }
      else
        {
        tipLinkLeftPos = tipLink2 ? tipLinkLeftPos + tipLinkWidth + 10 : tipLinkLeftPos + tipLinkWidth + 9;
        tooltipClass = tooltipClass + 'pointerLeft';
        }
      topOffset = tooltip.height()/2 - tipLinkHeight/2;
      break;
    } // end switch location

  topPos = tipLinkPos.top - topOffset;

  if(topPos > windowHeight) // when tabbing to tooltips, if tipLink is off-screen
    { timeoutVal = 3000; } // pause; page scrolls first, then tooltip vertically positions correctly
  else
    { timeoutVal = 0; }

  t = setTimeout(function(){

  // ensure tooltip fits in window vertically, adjust pointer
  windowObj = $(window);
  windowHeight = windowObj.height();
  tooltipTop = topPos - windowObj.scrollTop();
  tooltipHeight = tooltip.outerHeight();
  tooltipHeightHalf = tooltipHeight/2;
  pointerObj = tooltip.find('div.pointer');

  if((tooltipTop + tooltipHeight) > windowHeight) // tooltip crosses page bottom
    {
    if(dataTipLocation === 'bottom') // won't fit, reverse position
      {
      tooltipClass = tooltipClass.replace(/pointerTop/,'pointerBottom');
      topOffset = tooltipHeight + tipLinkHeight;
      topPos = tipLinkPos.top - topOffset;
      }
    else
      {
      topPos = topPos - (tooltipTop + tooltipHeight - windowHeight);
      tooltip.find('.pointer').css({'top':'auto','bottom':Math.max(windowHeight - tooltipTop - tooltipHeightHalf - pointerHeight/2,pointerStop)});
      }
    }
  else if(tooltipTop < 0) // tooltip crosses page top
    {
    if(dataTipLocation === 'top') // won't fit, reverse position
      {
      tooltipClass = tooltipClass.replace(/pointerBottom/,'pointerTop');
      topOffset = tipLinkHeight + 5;
      topPos = tipLinkPos.top + topOffset;
      }
    else
      {
      topPos = topPos - tooltipHeightHalf + (tooltipHeightHalf - tooltipTop);
      pointerObj.css('top',Math.max(tooltipTop + tooltipHeightHalf,9));
      }
    }


  // position the tooltip
  tooltip.addClass(tooltipClass);
  tooltip.css('top',topPos + 'px')
         .css(leftRight,tipLinkLeftPos + 'px');

  },timeoutVal);

  });

bodyObj.on('mouseleave','a.tipLink,a.tipLink2,a.infoLink,input.infoLink,input.tipLink,input.tipLink2',function(){
  var tId;
  // remove tip when not locked (hovered, allows links inside tip)
  tId = window.setTimeout(function(){ // allow time to hover tip
    if(!$('#tooltip').hasClass('tipLock')) { $('#tooltip').fadeOut(function(){$(this).remove();}); }
    }, 100);
  });

bodyObj.on('mouseenter','#tooltip',function(){
  $('#tooltip').addClass('tipLock'); // lock tip in place while hovering
  }).on('mouseleave','#tooltip',function(){
    $('#tooltip').removeClass('tipLock').fadeOut(function(){$(this).remove();});
  }); // close mouseleave

}; // close tooltip()

tooltip(); // invoke tooltip

bodyObj.on('click','a.tipLink,a.tipLink2,a.infoLink,input.infoLink,input.tipLink,input.tipLink2',function(event){
  event.preventDefault();
  $(this).blur();
});
} // end tip

// ++++++++++++++++++++++++
/* tooltip markup:

  using title for content:

  <a href="#" title="Simple and detailed views control the amount of information displayed on this chart." class="tipLink">What are these settings?</a>

  using separate element for content (shared by multiple tooltips):

  <a href="#tip-chart" class="tipLink">What are these settings?</a>
  <span id="tip-chart" class="hidden">
    Simple and detailed views control the amount of information displayed on this chart.
  </span>

*/
});





// Action Link
(function($){
	
	/**
	 *  @require jQuery 1.7
	 *  Creates a new Action Link
	 *  
	 *  @param ele element that will serve as action link
	 */
	function ActionLink(ele){
		if(!ele.length){ 
			throw new Error("Can't create ActionLink from undefined element.");
		}
		
		this.ele = ele;
		
		this.ele.data("jq-actions", this);
		
		this._init();
		this._show();
	}
	
	// keeps track of the current clicked action link
	var currentEle = null;
	
	ActionLink.prototype = {
			
		// TODO: add some description	
		show: function(){
			var self = this;
			
			var position = self._getPosition();
			
			self.tgtMenu.stop(true, false).css({
				position: "absolute",
				left: position.left,
				top: position.top
			}).fadeIn("fast", function(){
				self.isVisible = true;
			});
		},
		
		// TODO: add some description
		hide: function(){
			var self = this;
			this.tgtMenu.stop(true, false).fadeOut("fast", function(){
				self.isVisible = false;
				self.mouseOver = false;
				
				// just move it out of the way
				self.tgtMenu.css({
					left:-9999,
					top: -9999
				})
			});
		},
		
		/**
		 *  @private
		 */
		_init: function(){
			var tgt = this.ele.attr("href");
			var ieFixStart = tgt.indexOf('#'); // Fix IE issue with AJAX href attribute
			tgt = tgt.slice(ieFixStart);  // Fix IE issue with AJAX href attribute
			var tgtMenu = $(tgt);
			if(!tgtMenu.length){ 
				throw new Error("No target element found.");
			}
			
			this.tgtMenu = tgtMenu;
			
			var hideFunc = $.proxy(this._hide, this);
			var showFunc = $.proxy(this._show, this);
			var mouseOverFunc = $.proxy(this._onMenuMouseOver, this);
			
			var self = this;
			
			this.ele.bind("click", showFunc);
			this.ele.bind("mouseleave", hideFunc);
			this.ele.bind("mouseover", function(){
				if(self.hideTimer) clearTimeout(self.hideTimer);
			});
			
			this.tgtMenu.bind("mouseenter", mouseOverFunc);
			this.tgtMenu.bind("mouseleave", hideFunc);
			
			$(document.body).delegate(":not(.actionsMenu)", "click", function(e){
				self.hide.apply(self,[e]);
			});
		},
		
		/**
		 *  @private
		 */
		_onMenuMouseOver: function(){
			currentEle.addClass("active");	
			if(this.hideTimer) clearTimeout(this.hideTimer);
		},
		
		/**
		 *  @private
		 */
		_show: function(e){
			
			currentEle = this.ele;
			
			if(e) {
				e.preventDefault();
				e.stopPropagation();
			}
			var self = this;
			self.mouseOver = true;
			if(self.isVisible) return;
			if(this.showTimer) clearTimeout(this.showTimer);
			this.showTimer = setTimeout(function(){
				if(self.isVisible) return;
				self.show();
			}, 200);
		},
		
		/**
		 *  @private
		 */
		_hide: function(){
			var self = this;
			this.ele.removeClass("active");	
			self.mouseOver = false;
			if(self.isVisible){
				if(this.hideTimer) clearTimeout(this.hideTimer);
				if(this.showTimer) clearTimeout(this.showTimer);
				this.hideTimer = setTimeout(function(){
					if(self.isVisible && !self.mouseOver) self.hide();
				}, 200)
		
			}
		},
		
		/**
		 *  @private
		 */
		_getPosition: function(){
			var ele = this.ele;
			var tgtMenu = this.tgtMenu;
			var pos = ele.offset();
			return {
				top: pos.top + ele.outerHeight(),
				left: pos.left + ele.outerWidth() - tgtMenu.outerWidth()
			};
		}
		
	};
	
	ActionLink.prototype.constructor = ActionLink;
	
	//plugin
	$.fn.ActionLink = function(){
		return this.each(function(){
			new ActionLink($(this));
		});
	};
	
	
})(jQuery);






var TIAA = {};
TIAA.ui = (function($){
	
	/* TODO: "jQuery.browser" is deprecated, find another way
	   to achieve this functionality (Do we need this?) */
	blockStyle = jQuery.browser.msie ? "block" : "table-row";
	
	var SELECTORS = {
		"CURRENT_YEAR": "span.currentYYYY",
		"LAST_YEAR": "span.lastYYYY",
		"INPUT_DATE": "input.inputDate",
		"ACCORDION_HEADER": ".panels .hd",
		"ACCORDION_CONTENT": ".panels .content",
		"FILTERS": ".filters li a, .noticeModule .cancel, .infoModule .cancel",
		"FILTERS_LIST": ".filters",
		"FILTERS_CLEAR_ALL": ".breadBox .clearLink",
		"GLOBAL_MSG_CONTAINER": ".noticeModule",
		"GLOBAL_MSG_CLOSE_LINK": ".globalmsg .noticeModule a.expanded",
		"GLOBAL_ALERT_NOTICE": ".pageheader .alertNotice",
		"GLOBAL_ALERT_NOTICE_OPEN_LINK": ".pageheader .alertNotice a.collapsed",
		"SWITCH_VIEW": ".viewSwitch li, .ynSwitch li",
		"EXPAND_COLLAPSE_LINK": ".jsExpandCollapse",
		"EXPAND_COLLAPSE_HEADER_ROW": "tr",
		"EXPAND_COLLAPSE_DETAIL_ROW": "tr.detail",
		"MODAL_POPUP": ".jq-modal",
		"LOADER_MODAL": ".jq-loader"
	};
	
	var CLASSES = {
		"ACCORDION_HEADER_EXPANDED": "hdexpanded",
		"ACCORDION_CONTENT_EXPANDED": "bgexpanded",
		"ACCORDION_ICON_EXPANDED": "expanded",
		"ACCORDION_ICON_COLLAPSED": "collapsed",
		"SWITCH_VIEW_SELECTED": "selected",
		"EXPAND_COLLAPSE_EXPANDED": "expanded",
		"EXPAND_COLLAPSE_COLLAPSED": "collapsed",
		"EXPAND_COLLAPSE_TOP_BACKGROUND": "bgExpandedTop",
		"EXPAND_COLLAPSE_BOTTOM_BACKGROUND": "bgExpandedBottom"
		
	};
	
	var EVENTS = {
		"CLICK": "click"	
	};
	
	/**
	 *  Updates current year for the span element with "currentYYYY" class
	 *  Updates previous year for the span element with "lastYYYY" class
	 *  Note: might be a good idea to separate these into 2 different functions
	 */
	function initCopyrightDates(){
		$(SELECTORS.CURRENT_YEAR).html(new Date().getFullYear() + "");
		$(SELECTORS.LAST_YEAR).html((new Date().getFullYear() - 1) + "");
	}
	
	/**
	 *  Initializes datepicker widget for all input elements with "inputDate" class
	 *  @param opts jQuery UI datepicker widget options  
	 */
	function initDatePicker(opts, container){
		var options = $.extend({},{
			constrainInput: true,
			min: '+1'
		}, opts);
		//var perpend = (!container ? "div:not(.popup) + " : "");
		var prepend = "";
		$(prepend+SELECTORS.INPUT_DATE, container || document).each(function(){var $this = $(this), x = $.extend({}, {changeMonth: ($this.attr('data-changeMonth') == 'true' ? true : false), changeYear: ($this.attr('data-changeYear') == 'true' ? true : false)}, options); $this.datepicker(x);})
		$(prepend+".input-calendar", container || document).each(function(){var $this = $(this), x = $.extend({}, {changeMonth: ($this.attr('data-changeMonth') == 'true' ? true : false), changeYear: ($this.attr('data-changeYear') == 'true' ? true : false)}, options); $this.datepicker(x);})
	}
	
	/**
	 *  Initializes expand/collapse behavior for the accordion panels.
	 *  Use this function to bind expand/collapse behavior with the header of the panel.
	 */
	function initAccordionWithHeader(){
	var $pn = $('div.panels');
	$pn.each(function(e){
		var className = $(this).attr('class'),
		$hdr = $(this).find($(SELECTORS.ACCORDION_HEADER)),
		$anc = $hdr.find('a'),
		$cntnt = $(this).find($(SELECTORS.ACCORDION_CONTENT));

		if(className.indexOf('acco') !== -1 && className.indexOf('panels') !== -1 ){	
			$hdr.find('a').attr('class',CLASSES.ACCORDION_ICON_COLLAPSED);
			$cntnt.addClass(CLASSES.ACCORDION_CONTENT_EXPANDED).hide().eq(0).show();
			$hdr.eq(0).addClass('hdexpanded');
			$anc.eq(0).attr('class',CLASSES.ACCORDION_ICON_EXPANDED);

			//Added for accordion
			if($('div.jq-accordions').length){
				var $self = $(this);			
				$(this).accordion({header:'div.hd',autoHeight: false,
					create: function(eve, ui){
						var mdata = $(this).data('chart');
						if(mdata){
							var startNewChart = new StartNewChart (this, eve, mdata);
							startNewChart.init();
						};
					},
					/* Added event activate for newer version of jQuery UI */
					activate: function(eve, ui){			
					     	var $this= $(this), mdata = $this.data('chart'), i = $this.find('div.hd').index(ui.newHeader[0]);
					     	if(mdata){
							var startNewChart = new StartNewChart (ui, eve, mdata, i);
							startNewChart.init();
						};			     	
					}, 
					change: function(eve, ui){			
					     	var $this= $(this), mdata = $this.data('chart'), i = $this.find('div.hd').index(ui.newHeader[0]);
					     	if(mdata){
							var startNewChart = new StartNewChart (ui, eve, mdata, i);
							startNewChart.init();
						};			     	
					}
				}).find($hdr).click(function(){
					$self.find($(SELECTORS.ACCORDION_HEADER)).removeClass(CLASSES.ACCORDION_HEADER_EXPANDED).find('h3>a').attr('class',CLASSES.ACCORDION_ICON_COLLAPSED); 					
					$(this).addClass(CLASSES.ACCORDION_HEADER_EXPANDED).find('h3>a').attr('class',CLASSES.ACCORDION_ICON_EXPANDED);
				});
			};		
		}else{			
		// Expand Collapse Panel
			$(this).find('div.content').hide();
			$.each($anc, function(){
				if($(this).attr('class')=== 'expanded'){
					$(this).closest('div.hd').addClass(CLASSES.ACCORDION_HEADER_EXPANDED);
					$(this).closest('div.hd').next().slideDown();
				};
			});
			$hdr.toggle(function() {
				$(this).addClass(CLASSES.ACCORDION_HEADER_EXPANDED).next().addClass(CLASSES.ACCORDION_CONTENT_EXPANDED).slideDown();
				$(this).find('>h3>a').attr('class',CLASSES.ACCORDION_ICON_EXPANDED);
					
			},function(){	
				$(this).removeClass(CLASSES.ACCORDION_HEADER_EXPANDED).next().removeClass(CLASSES.ACCORDION_CONTENT_EXPANDED).slideUp();
				$(this).find('>h3>a').attr('class',CLASSES.ACCORDION_ICON_COLLAPSED);
			});
			$(this).find('a.expanded').eq(0).trigger('click');
		};
	});
	
 	};	
	
	/**
	 *  Initializes filter close behavior
	 */
	function initFilters(){
		//$(SELECTORS.FILTERS).live(EVENTS.CLICK, function(e){
//			e.preventDefault();
//			$(this).parent().fadeTo('fast', 0, function() {
//				$(this).slideUp('fast');
//			});
//		});
         $(SELECTORS.FILTERS).live(EVENTS.CLICK, function(e){
            e.preventDefault();
            $(this).parent().fadeTo('fast', 0, function() {
            $(this).slideUp('fast').addClass('closed'); // add closed class to easily get length of list
            	if($(this).parent().children('li:not(".closed")').length == 0){ // hide section when all list items are closed
				$(this).parent().parent().parent().slideUp('fast', function(){ 
                	$(this).removeClass('visible'); // remove visible class from notifications module
                });
              }
           });
        });
		$(SELECTORS.FILTERS_CLEAR_ALL).live(EVENTS.CLICK, function(e){
			e.preventDefault();
			$(this).parent().parent().fadeTo('fast', 0, function() {
				$(this).slideUp('fast');
			});
		});
	}
	
	/**
	 *  Initializes global alert close and open behavior
	 */
	function initGlobalAlert(){
		
		$(SELECTORS.GLOBAL_MSG_CLOSE_LINK).live(EVENTS.CLICK, function(e) {
			e.preventDefault();
			$(this).blur().closest(SELECTORS.GLOBAL_MSG_CONTAINER).slideUp('fast',function(){
				$(SELECTORS.GLOBAL_ALERT_NOTICE).slideDown('fast');
			});
		});
		
		$(SELECTORS.GLOBAL_ALERT_NOTICE_OPEN_LINK).live(EVENTS.CLICK, function(e) {
			e.preventDefault();
			var ele = $(this);
			ele.blur();
			$(SELECTORS.GLOBAL_MSG_CONTAINER).slideDown('fast');
			ele.closest(SELECTORS.GLOBAL_ALERT_NOTICE).slideUp('fast');
		});
	}
	
	
	/**
	 *  Initializes switch buttons (Including custom and yes/no)
	 */
	function initSwitchView(){
		$(SELECTORS.SWITCH_VIEW).live(EVENTS.CLICK, function(e){
			e.preventDefault();
			$(this).parent().find("li a").removeClass(CLASSES.SWITCH_VIEW_SELECTED)
							.end().end().children().first().addClass(CLASSES.SWITCH_VIEW_SELECTED);
		});
	}
	
	/**
	 *  Initialize expand/collapse behavior for anchor element with class "jsExpandCollapse"
	 *  Note: prototype pages are currently using inline call to "showHideTRs" function,
	 *  use this new implementation instead
	 */
	function initExpandCollapse(){
		$(SELECTORS.EXPAND_COLLAPSE_LINK).live(EVENTS.CLICK, function(e){
			e.preventDefault();
			var ele = $(this);
			ele.toggleClass(CLASSES.EXPAND_COLLAPSE_EXPANDED + " " + CLASSES.EXPAND_COLLAPSE_COLLAPSED);
			var headerRow = ele.closest(SELECTORS.EXPAND_COLLAPSE_HEADER_ROW);
			var detailRow = $(ele.attr("href"));
			detailRow.toggle("fast", function(){
				detailRow.find('td').first().toggleClass(CLASSES.EXPAND_COLLAPSE_BOTTOM_BACKGROUND);
				headerRow.find('td').toggleClass(CLASSES.EXPAND_COLLAPSE_TOP_BACKGROUND);
			});
			
		});
	}
	
	
	function initSimpleExpandCollapse(){
		$(".jq-expand-collapse").live(EVENTS.CLICK, function(e){
			e.preventDefault();
			var ele = $(this);
			var tgtEle = $(ele.attr("href"));
			if(ele.hasClass("collapsed")){
				ele.removeClass("collapsed").addClass("expanded");
				tgtEle.slideDown();
			}else{
				ele.removeClass("expanded").addClass("collapsed");
				tgtEle.slideUp();
			}
		});
	}
	
	function setTab(link, event) {
	if(event) {
		event.preventDefault();
	}

	var ulobj = $(link).closest('ul');

	$(ulobj).find('a').removeClass('selected');
	$(link).addClass('selected').blur();

	var duration = ulobj.hasClass('fading') ? 'slow' : 0;
	$('a', ulobj).each(function(i, item) {
		var tabobj = $(item).attr('rel');
		$(tabobj).hide();
	});
	$($(link).attr('rel')).fadeTo(duration, 1);
	}

		// Handle tab selection
	$('ul.tabs > li > a').live('click', function(e) {
		setTab(this, e);
	
		//Get Location of tab's content
		var contentLocation = $(this).attr('href');
	
		//Let go if not a hashed one
		if(contentLocation){

			if(contentLocation.charAt(0)=="#") {
	
				e.preventDefault();
	
				//Make Tab Active
				$(this).parent().siblings().children('a').removeClass('selected');
				$(this).addClass('selected');
	
				//Show Tab Content & add active class
				$(contentLocation).show().addClass('selected').siblings('.bd').hide().removeClass('selected');
			}
		}
	});
	
	function initTabs(){
		// Use jquery to hide all tab content initially in IE6
		if($.browser.msie && $.browser.version == 6){
			$('.tabs-container ul.tabs~div').css('display','none');
		}
		
		// Show tab content initially
		$('ul.tabs').each(function(){
			var initialContentLocation = $(this).find('li a.selected').attr('href');
			var initialContentLocationRel = $(this).find('li a.selected').attr('rel');
			if(initialContentLocation){
				if(initialContentLocation.charAt(0)=="#") {
					$(initialContentLocation).show().siblings('.bd').hide();
				} else if(initialContentLocationRel.charAt(0)=="#") {
					$(initialContentLocationRel).show().siblings('.bd').hide();
				}
			}
		});
		


	}
	// End tab functionality
		
	/**
	 *  Initializes modal popup open/close behavior for all modals.
	 *  The markup should have a class "widget-modal" and href attribute
	 *  should point to the id (#id) of the content div
	 */


function configurePopup() {
// configure popup with standard classes
//$('div.ui-dialog').bgiframe(); // hide select form element in IE6
//$('div.ui-dialog-titlebar').addClass('head');
$('div.ui-dialog-titlebar').addClass('head').attr('tabindex',-1);
$('span.ui-dialog-title').addClass('popup-title');
$('a.ui-dialog-titlebar-close').addClass('popup-close');
$('div.ui-draggable').addClass('popup-draggable');
$('span.ui-icon').addClass('popup-close-icon');
if($('div.ui-dialog:visible').hasClass('modal'))
  {
  $('div.ui-widget-overlay').addClass('modal-overlay');
  }
} // end fun

// had to move event handler outside of the createPopup function so that it's static.
// it's used for unbinding the event without unbinding all other click events.
var poplogic = function(event) {

if(event.type !== 'keypress' || event.which === 13) // allow tabbing
  {

  var thisObj = $(this);
  if(thisObj.is('a')) { event.preventDefault(); } // prevent anchor behavior
  thisObj.blur();

  var mypopup = thisObj.data('popup');
  if (mypopup.filter(SELECTORS.LOADER_MODAL).length) {
    var counter = 0;
    var self = $(this);
    var total = parseInt(mypopup.attr('data-total')) || 5;
    mypopup.find('.current').html('0');
    mypopup.find('.total').html(total);
    var timer;
    mypopup.find('.progress').progressbar({value: 0});
    var timerHandler = function() {
      if (counter/total < 1) {
        counter++;
        mypopup.find('.progress').progressbar({value: ((counter/total) * 100)});
        mypopup.find('.current').html(counter);
        mypopup.trigger('loader-step', [counter, total]);
      } else {
        mypopup.find('.progress').progressbar({value: 100});
        mypopup.find('.current').html(counter);
        clearInterval(timer);
        mypopup.trigger('loader-complete');
        var has_loader_event = false;
        for (var event in mypopup.data('events')) {
        	if (event == 'loader-complete') {
        		has_loader_event = true;
        		break;
        	}
        };
        if (!has_loader_event) {
            window.location.href = self.attr('href');
        }
        mypopup.dialog('close');
      }
    };
    timer = setInterval(timerHandler, parseInt(mypopup.attr('data-frequency')) || 250);
    mypopup.bind('dialogclose', function(event, ui){
        clearInterval(timer);
    });

  }

  mypopup.data('openerObj',thisObj); // track for accessibility: return focus on close
  mypopup.dialog('open');
  } // end accessibility
};

function createPopup(popupElements) {
"use strict";

/* Added to check for boolean options passed as strings that jQuery Dialog requires to be boolean */

function isStringBool(str){
var test;
	if(str.toLowerCase()==='false'){
		test = false;
	}
	else if (str.toLowerCase()==='true')
	{
		test = true;
	} 
	else
		{ // a boolean was not passed, return value as is 
			test = str;
	}
	return test;
}

popupElements.each(function(){
  var popup,popupCustomOptions,popupData,popupDefaultOptions,popupOptions,popupField,popupId;

  popup = $(this);
  popupId = popup.attr('id');

  // specify default popup dialog options
  popupDefaultOptions = {
          autoOpen:false,
          close:function(){
	    $(this).trigger('dc-close-modal');
            //if(popup.data('openerObj') !== '')
             // { popup.data('openerObj').focus(); } // return focus to opening button
            },
          closeOnEscape:true,
          closeText:'Close',
          dialogClass:'modal', // standard class
          draggable:false,
          hide:{effect:"fade",duration:600},
          minHeight:20,
          modal:true,
          open:function(){
            configurePopup();
            $(this).trigger('dc-load-modal');
            },
          resizable:false,
          show:{effect:"fade",duration:600},
          width:600 // not adjusted for css borders
          };

  // get optional custom popup options specified in data-popup attribute
  // format = data-popup="width:200,draggable:true"
  popupCustomOptions = {};
  if(popup.is('[data-popup]'))
    {
    popupData = popup.attr('data-popup').split(',');
    $.each(popupData,function(){
	    popupField = this.split(':');
      popupCustomOptions[popupField[0]] = isStringBool(popupField[1]);
    });
    }

  // combine default and custom options (custom values override default values)
  popupOptions = $.extend({},popupDefaultOptions,popupCustomOptions);

  // add values to adjust for css-defined borders
  //   makes width/height calculations unnecessary for developer
  popupOptions.width = parseInt(popupOptions.width,10) + 12;
  if(popupOptions.height)
    { popupOptions.height = parseInt(popupOptions.height,10) + 42; }

  // configure popup
  popup.dialog(popupOptions);
  // add event to anchor to open popup (href = popup '#'+id)
  // also unbinding previously bound popup events - this is so recurring calls to initialize popups don't result in multiple instances popping up
  $('a[href="#'+popupId+'"],a[data-loader="#'+popupId+'"],input[data-behavior="'+popupId+'"],button[data-behavior="'+popupId+'"]').data('popup', popup).unbind('click keypress', poplogic).on('click keypress', poplogic);

}); // end each popup

$('a.clickHelp,a.clickHelpLink').on('click',function(){
  if(event.type !== 'keypress' || event.which === 13)
    {
    event.preventDefault();
    var thisClick = $(this);
    thisClick.blur();
    } // end accessibility
  });

} // end create popup
//


	function initModalPopup(){

          if($('div.popup,form.popup').length > 0)
            {
            createPopup($('div.popup,form.popup'));
            }

// ++++++++++++++++++++++++
// generic, reusable close

/*
$('a.closePopup, a[href="#closePopup"], input.closePopup').on('click',function(event){
  event.preventDefault();
  var popupObj = $(this).closest('div[class~=popup]');
  if(popupObj.attr('id') === undefined)
    { popupObj = $(this).closest('form[class~=popup]'); }
  popupObj.dialog('close');
});
*/

$('a.closePopup,a[href="#closePopup"],input.closePopup,input[data-behavior~="closePopup"],button.closePopup,button[data-behavior~="closePopup"]').on('click keypress',function(event){
  if(event.type !== 'keypress' || event.which === 13)
    {
    if($(this).is('a')) { event.preventDefault(); } // prevent anchor behavior
    var popupObj = $(this).closest('div[class~=popup]');
    if(popupObj.attr('id') === undefined)
      { popupObj = $(this).closest('form[class~=popup]'); }
    popupObj.dialog('close');
    } // end accessibility
});

	};
	


	// For Expand / Collapsed All
	
	var initExpandAll = (function() {
		$(".jq-expand-all").click(function(e) {e.preventDefault();
			var ele = $(this);
			ele.parent().next().find('div.hd').each(function() {
				(ele.is('.addLink'))? 
					(!$(this).is('.hdexpanded') && $(this).trigger('click')):
					($(this).is('.hdexpanded') && $(this).trigger('click'));
			});
			ele.toggleClass('addLink minusLink');
		});
	});


	
// Remove Tooltips Initializes function


	/**
	 *  Initializes behavior for action links for elements with class "jq-actions"
	 */
	function initActionLinks(){
		var currentMenu = null;
		$(document).on("click", ".jq-actions", function(e){
			e.preventDefault();
			$(this).ActionLink();
		});
	}
	
	/** 
	 * Initializes  show more link behavior for element with wrapper div.more-less
	 * Will hide all li elements with index > 3 and toggle them when the show more link is clicked
	 */
	function initShowMoreLink(){
		$('.more-less').each(function(i, item) {
			var $navlist = $(item);			
			$('li:gt(2)', $navlist).hide();
			var link = $('<a href="#" class="toggle addLink"><span class="icon"></span>show more</a>');
			$navlist.append(link);
			$navlist.addClass('collapsed');
		});
		
		$('.more-less .toggle').click(function(event) {
			event.preventDefault();
			event.stopPropagation();
			var el = $(this);
			el.blur();

			var ml = $(this).closest('.more-less');
			ml.toggleClass('collapsed expanded');
					
			var linkText = 'show more';
			var iconClass = 'addLink';
			if (ml.hasClass('expanded')) {
				$('li', ml).show();
				linkText = 'show less';
				iconClass = 'minusLink';
			} else {
				$('li:gt(2)', ml).hide();
			}
			el.removeClass('addLink minusLink').addClass(iconClass);
			el.html("<span class='icon'></span>" + linkText);
		});
	}
	
// Combo Box
$(function(){
	var _combo = $('select.comboBox-source');
	if(_combo.length){
		_combo.each(function(){
			var _combo=Object.create(initComboBox);
	 		_combo.init($(this));
		});		
	};

})

initComboBox = (function($){
	var comboObj = {

		init: function(obj){
			this.obj=obj;
			this.targetObj  = this.obj.prev();
			this.selVal     = this.obj.find('option:selected');
			this.optval     = $('option',this.obj);
			this.dlTemp 	= '<dl class="comboBox"></dl>';
			this.dtTemp		= '<dt><a  role="menuitem" tabindex="0"><span class="icon"></span>' + this.selVal.text() + '<span class="value">' + this.selVal.val() + '</span></a></dt>';
			this.ddTemp     = '<dd><ul role="menu"  aria-hidden="true"  aria-haspopup="true"></ul></dd>';
			this.runAll();

		},
		
		runAll: function(){
			this.onMakeCombo();
			this.showDropdown();
			this.selectElement();
			this.bodyEvent();
		},
		
		onMakeCombo: function(){
			this.targetObj.append(this.dlTemp);
			this.targetObj.find("dl").append(this.dtTemp).append(this.ddTemp);
			this.targetObj.find("ul").append(this.optionTemplate());
			this.targetObjUl=this.targetObj.find("ul");
		},
		optionTemplate : function(){
			var liTag='';
			this.optval.each(function(index){
				liTag+="<li  role='presentation'>"
						+"<a href='#' role='menuitem' tabindex='0'>"
						+$(this).text()
						+"<span class='value'>"+ $(this).val()+ "</span></a></li>"
			
			
			});
		    return liTag;
		},
		showDropdown : function(){
			var self=this;
			this.targetObj.on('click keydown','dt a' , function(e){
				var keycode = (e.keyCode ? e.keyCode : e.which);
				if(keycode == '13'){
					e.preventDefault();
				}	
				if(keycode == '13'||e.type=="click"){
					self.targetObjUl.toggle();
					self.ariaAttFunc(self.targetObjUl);
					self.targetObjUl.find("li a:first").focus();
			    }
			});
		},
		ariaAttFunc : function(self){
			if($(self).is(":visible")){
					$(self).attr('aria-hidden','false');
				}
				else{
					$(self).attr('aria-hidden','true');
				}
		},
		selectElement : function(){
			var self=this;
			this.targetObj.on('click keydown','dd a', function(e){
				var keycode = (e.keyCode ? e.keyCode : e.which);
				if(keycode=='27'){
					self.targetObjUl.hide().attr('aria-hidden','true');
				}	
				if(keycode=='38'){
					e.preventDefault();
					$(this).removeClass('hovered');
					$(this).parent().prev().children().addClass('hovered').focus();
				}
				if(keycode=='40'){
					e.preventDefault();
					$(this).removeClass('hovered');
					$(this).parent().next().children().addClass('hovered').focus();
				}
				if(keycode == '13'|| keycode == '32'|| e.type=='click'){
					var thisClick;
					thisClick = $(this);
					e.preventDefault();
					self.targetObj.find("dt a").html(thisClick.html()+'<span class="icon"></span>').focus();
					self.targetObjUl.hide().attr('aria-hidden','true');
					self.targetObj.next().val(thisClick.find('span.value').html());
					self.targetObj.removeClass('hovered');
				}
			});

		},
		bodyEvent : function(){
			$('body').on('click' ,function(e) {
			  	if(!$(e.target).parents().hasClass('comboBox'))
			   			{ $('dl.comboBox dd ul').hide().attr('aria-hidden','true');
			   			}
			});	
		}


	};

	return comboObj;
})(jQuery);
// end fun

function initAccessibility(){
"use strict";

//  skip to main content (skip navigation)
// wrap skip anchor in a div for IE, add appropriate attributes to target
$('body').children(':first').before('<div id="skip"><a href="#pagecontent">Skip to Main Content</a></div>'); // preferred text for screen readers
$('div.pagecontent:first').attr({'id':'pagecontent',
                                 'role':'main',
                                 'tabindex':-1
                                });
//

//$('html.ie6').find('[aria-hidden="true"]').hide(); // ie6 ignores css

$(':tabbable:first').focus(function() { // IE ensure modal is accessible when tabbing
  $('div.ui-dialog:visible:last :tabbable:first').focus();
});
} // end fun
	
	/**
	 *  Public API
	 */
	return {
		initAccessibility: initAccessibility,
		initCopyrightDates: initCopyrightDates,
		initDatePicker: initDatePicker,
		initAccordionWithHeader: initAccordionWithHeader,
		initFilters: initFilters,
		initGlobalAlert: initGlobalAlert,
		initSwitchView: initSwitchView,
		initExpandCollapse: initExpandCollapse,
		initModalPopup: initModalPopup,
		initSimpleExpandCollapse: initSimpleExpandCollapse,
		initTabs: initTabs,
		initExpandAll: initExpandAll,
		// initTooltips: initTooltips,
		initActionLinks: initActionLinks,
		initShowMoreLink: initShowMoreLink,
		//initComboBox: initComboBox,
		initAll: function(){
			$(document).bind("dc-after-load", function(){
				initAccessibility();
				initCopyrightDates();
				initDatePicker();
				initAccordionWithHeader();
				initFilters();
				initGlobalAlert();
				initSwitchView();
				initExpandCollapse();
				initSimpleExpandCollapse();
				initModalPopup();
				initTabs();
				initExpandAll();
				//initTooltips();
				initActionLinks();
				initShowMoreLink();
				//initComboBox();
			});
		}
	}
	
})(jQuery);


// initialize
jQuery(document).ready(function($) {

	TIAA.ui.initAll();

});

//
//
// ****************
// shared functions

function showHide(obj) {
	if (obj.is(':hidden')) {
		obj.removeClass('hidden');
	} else {
		obj.addClass('hidden');
	}
}

/* NOTE: Why do we have the same function again,
   there is no function overloading in JavaScript.
   The interpreter is just going to use the
   last found function. Which one do we need?

function showHide(shObj) {
	$(shObj).slideToggle('fast');
	$(shObj).toggleClass('bgexpanded');
}*/

function dollarFormatter(v, axis) {
	v += '';
	x = v.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return '$' + x1 + x2;
}


// For features carousel on the right
$(document).bind('dc-after-load', function(){
	if ($('.features-slider ul').length) {
		$('.features-slider ul').each(function(){
			
			var ele = $(this);
			var items = ele.find(">li");
			
			//var height = items.first().outerHeight();	
//			ele.height(height);
			
			var headers = ele.find("h4");
			
			headers.css({
				position: 'absolute',
				top: 0,
				left: 0	
			}).width(ele.first().width() - (headers.first().outerWidth() - headers.first().width()) - 2);
			
			// create the pager markup
			var pagerHtml = [];
			pagerHtml.push("<div class='bx-pager'>");
			var len = items.length;

			for(var i = 0; i < len; i++){
				pagerHtml.push("<a href='javascript:void(0)' class='pager-link' data-index='");
				pagerHtml.push(i);
				pagerHtml.push("'>");
				pagerHtml.push(i + 1);
				pagerHtml.push("</a>");
			}

			pagerHtml.push("</div>");
			ele.parent().append(pagerHtml.join(""));

			var pagerItems = ele.parent().find('.bx-pager > a');
			
			pagerItems.click(function(e){
				e.preventDefault();
				goToNextItem(parseInt($(this).attr('data-index'), 10));
			});

			var curIndex = 0;

			items.hide();
			items.first().show();
			pagerItems.first().addClass("pager-active");
			
			var timerRef;	
	
			function goToNextItem(itemIndex){
				
				if(timerRef) clearTimeout(timerRef);
				
				$(items[curIndex]).fadeOut(1500);
				$(pagerItems[curIndex]).removeClass("pager-active");
				
				if(typeof(itemIndex) != 'undefined' && itemIndex >= 0){	
					curIndex = itemIndex;
				}else{
					curIndex = curIndex + 1;
				}
				
				if(curIndex === len) curIndex = 0;

				$(items[curIndex]).fadeIn(1500, function(){
					timerRef = setTimeout(function(){
						goToNextItem();
					}, 10000);
				});	
				$(pagerItems[curIndex]).addClass("pager-active");
				
			}
	
			timerRef = setTimeout(goToNextItem, 10000);	
				
		});	
	}	
	
});

/*
 * jQuery Plugin to obtain touch gestures
 * @author Andreas Waltl, netCU Internetagentur (http://www.netcu.de)
 */
(function($){$.fn.touchwipe=function(settings){var config={min_move_x:20,min_move_y:20,wipeLeft:function(){},wipeRight:function(){},wipeUp:function(){},wipeDown:function(){},preventDefaultEvents:true};if(settings)$.extend(config,settings);this.each(function(){var startX;var startY;var isMoving=false;function cancelTouch(){this.removeEventListener('touchmove',onTouchMove);startX=null;isMoving=false}function onTouchMove(e){if(config.preventDefaultEvents){e.preventDefault()}if(isMoving){var x=e.touches[0].pageX;var y=e.touches[0].pageY;var dx=startX-x;var dy=startY-y;if(Math.abs(dx)>=config.min_move_x){cancelTouch();if(dx>0){config.wipeLeft()}else{config.wipeRight()}}else if(Math.abs(dy)>=config.min_move_y){cancelTouch();if(dy>0){config.wipeDown()}else{config.wipeUp()}}}}function onTouchStart(e){if(e.touches.length==1){startX=e.touches[0].pageX;startY=e.touches[0].pageY;isMoving=true;this.addEventListener('touchmove',onTouchMove,false)}}if('ontouchstart'in document.documentElement){this.addEventListener('touchstart',onTouchStart,false)}});return this}})(jQuery);


(function($){	// START  <== Show more or less  
	var ShowMoreLess = {
		init: function(elem){
			var my = this; my.$elem = $(elem); my.more = my.$elem.attr('data-more') || 'Show More'; my.less = my.$elem.attr('data-less') || 'Show Less'; my.def = my.$elem.attr('data-default') || 'close'; my.mySpeed = my.$elem.attr('data-speed')*1000 || 400;
			my.flag = (my.def==="close") ? 0 : 1;
			my.temp = $.trim('<a class="tc-showmorelink {{cssState}}" title="{{title}}" href="#"><span class="icon"></span><span class="text"> {{link}}</span></a>');
			my.flagging();
		},
		flagging: function(){ var my = this;
			(my.flag === 0) ? (function(){my.anchClass = 'addLink';my.$elem.hide();my.act = my.more;})():
					(function(){my.anchClass = 'minusLink';my.$elem.show();my.act = my.less;})();
		},
		construct: function(){var my = this;
			my.anch = $(my.temp.replace(/{{cssState}}/ig, my.anchClass).replace(/{{title}}/ig, my.act).replace(/{{link}}/ig, my.act)).insertAfter(my.$elem).on('click', function(e){e.preventDefault(); my.clickFunc.call(my)});
		},
		clickFunc: function(){var my = this;
			my.$elem.slideToggle(my.mySpeed);
			if(my.anch.hasClass('minusLink')){
				my.anch.removeClass('minusLink').addClass('addLink').attr('title',my.more).find('span.text').text(my.more);
			}else{
				my.anch.removeClass('addLink').addClass('minusLink').attr('title',my.less).find('span.text').text(my.less);
			};
		}
	};
	$.fn.showMoreLess = function(){
		return this.each(function(i){
			 var showMR = Object.create(ShowMoreLess);
			 showMR.init(this); showMR.construct();
		});
	};
	$(function(){ 
		$('.tc-showmore').showMoreLess(); // Initiating show More/Less
	});
})(jQuery); 	// END  <== Show more or less 