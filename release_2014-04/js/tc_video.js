// widgets 05-17-2012

//Video Player for Brightcove video's

        var showModal = function (content, options) {
        content = $(content);
                options = options || {};

                //$('.ui-dialog-content').dialog('destroy').empty();

                //popup = content.clone();
                popup = content;

                var defaults = {
                        autoOpen: false,
                        closeOnEscape: false,
                        closeText: 'Close',
                        dialogClass: 'modal',
                        showClose: false,
                        draggable: false,
                        minHeight: 20,
                        modal: true,
                        open: null,
                        close: null,
                        resizable: false,
                        height: 350,
                        width: 600,
                        position: 'center'
                };

                var settings = $.extend({}, defaults, options);

                var popupCustomOptions = {};
                if(popup.is('[data-popup]'))
                  {
                  popupData = popup.attr('data-popup').split(',');
                  $.each(popupData,function(index,value){
                    popupField = value.split(':');
                    popupCustomOptions[popupField[0]] = popupField[1];
                  });
                  }

                settings = $.extend({}, settings, popupCustomOptions);


                // clean up width / height
                settings.width = parseInt(settings.width) + 12;
                if (settings.height) {
                        settings.height = parseInt(settings.height) + 42;
                }

                settings.open = function(event, ui) {
                        if (typeof(options.open) === 'function') {
                                options.open.apply(this);
                        }
                };
                settings.close = function(event, ui) {
                        if (typeof(options.close) === 'function') {
                                options.close.apply(this);
                        }

                        $(this).remove();
                }
                popup.dialog(settings);
                popup.dialog('open');
        }



$('.jq-modal-video').live('click', function(e) {
	e.preventDefault();
	$(this).blur();

	var el = $(this);
	var width = el.data('width');
	var height = el.data('height');
	var title = el.data('title');

	var popup = $(
		'<div class="popup" title="New Account Overview">' +
			'<div class="body np">' +
				'<div id="video-holder"></div>' +
			'</div>' +
		'</div>'
	);
  
	$('#video-holder', popup).css({
		'width': width + 'px',
		'height': height + 'px'
	});
  



	//TIAA.ui.showModal(popup, {
	showModal(popup, {
		title: title,
		width: width,
		height: height,
		open: function() {

			options = {};

			var playerID = el.data('playerid');
			var playerKey = el.data('playerkey');
			var videoPlayer = el.data('videoplayer');

			if (typeof(playerID) !== 'undefined') { options.playerID = playerID; }
			if (typeof(playerKey) !== 'undefined') { options.playerKey = playerKey; }
			if (typeof(videoPlayer) !== 'undefined') { options.videoPlayer = videoPlayer; }
			if (typeof(width) !== 'undefined') { options.width = width; }
			if (typeof(height) !== 'undefined') { options.height = height; }
      
			options.autoStart = true;
			$('#video-holder').addBCVideo(options);

		}, 
		close: function() {
			$('#video-holder').empty();     
		}
	});
});

(function($) {
	$.fn.addBCVideo = function(options) {

		var defaults = {
			playerID : '', 
			playerKey : '',
			videoPlayer : '1215051621001',
			width : '600',
			height : '338',
			autoStart : 'false'
		};
      
		this.each(function(i, item) {

			var $item = $(item);
			var localoptions = options;

			if (typeof(localoptions) === 'undefined') {
				localoptions = {};
  
				var playerID = $item.data('playerid');    // this will be deprecated
				var playerKey = $item.data('playerkey');
				var videoPlayer = $item.data('videoplayer');
				var width = $item.data('width');
				var height = $item.data('height');
				var autoStart = $item.data('autostart');
        
				if (typeof(playerID) !== 'undefined')       { localoptions.playerID = playerID; }
				if (typeof(playerKey) !== 'undefined')    { localoptions.playerKey = playerKey; }
				if (typeof(videoPlayer) !== 'undefined')    { localoptions.videoPlayer = videoPlayer; }
				if (typeof(width) !== 'undefined')        { localoptions.width = width; }
				if (typeof(height) !== 'undefined')       { localoptions.height = height; }
				if (typeof(autoStart) !== 'undefined')    { localoptions.autoStart = autoStart; }
			}

			var settings = $.extend({}, defaults, localoptions);
      
			var flash = '<object class="BrightcoveExperience">\
				<param name="bgcolor" value="#000000" />\
				<param name="autoStart" value="' + settings.autoStart + '" />\
				<param name="width" value="' + settings.width + '" />\
				<param name="height" value="' + settings.height + '" />\
				<param name="playerID" value="' + settings.playerID + '" />\
				<param name="playerKey" value="' + settings.playerKey + '" />\
				<param name="isVid" value="true" />\
				<param name="isUI" value="true" />\
				<param name="dynamicStreaming" value="true" />\
				<param name="@videoPlayer" value="' + settings.videoPlayer + '" />\
				<param name="wmode" value="transparent" />\
			</object>';
			$item.append(flash);
		});
		brightcove.createExperiences();
	};
})(jQuery);


// initialize
$(document).ready(function() {
	$('.jq-bc-video').addBCVideo()
});
