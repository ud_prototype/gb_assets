/**
    Version: 1.1
    Simple form validation jQuery plugin built for TIAA-CREF forms with style guide standards.
    This can be extended/overridden by writing customized rules along with pre-defined rules like email, url, etc., 
    This plugin works only when "Form Validation option" is enabled in prototype (Custom) settings.
**/
(function(){
    var utils = {
        _getElements: function($form){
            var $allElements = $form.find("input:not(:submit):not(:button):not(:reset):not([type='hidden']), select, textarea"),
                $visibleElements = $allElements.filter(function(){
                    var $this = $(this);
                    return ($this.is(":visible") || ($this.is(":not(:visible)") && $this.parents("[data-tovalidate='true']")).length);
            });
            return $visibleElements;
        },
        _isRequired: function($el){
            return ($el.attr("aria-required") === "true" || $el.attr("required")) ? true : false
        },
        _errorHandler: function(){
            var self = this,
                els = self[0].elements,
                settings = self.settings,
                $parent = $(settings.parent),
                $alertModule = $parent.find(".alertModule:eq(0)"),
                errorList = [];
            $alertModule.find('ul').empty();
            for(var i = 0; i< els.length; i++){
                $el = $(els[i]);
                
                if(typeof $el[0].tcvalidation === "object"){
                    var $vobj = $el[0].tcvalidation;
                    
                    if($vobj["error"]){
                        $el.parents('.lblFieldPair, .lblFieldPairV, .lblSelectPair').addClass('alertHighlight');
                        if($vobj["showmessage"]){
                            if(settings.message === "both" || settings.message === "top"){
                                $alertModule.find('ul').append("<li><span>"+$vobj["message"]+"</span></li>");
                                errorList.push($el.attr("name"));
                            }
                            if(settings.message === "both" || settings.message === "inline"){
                                $el.parents(".input").append("<span class='alertText'>"+$vobj["message"]+"</span>");
                                errorList.push($el.attr("name"));
                            }
                        }
                        if($el.parents("[data-tovalidate='true']").length){
                            var $tovalidateParent = $el.parents("[data-tovalidate='true']");
                            if($tovalidateParent.data("show") === true)
                                $tovalidateParent.removeClass("hidden").show();
                        }
                    }
                    else{
                        $el.nextAll(".icsuccess").remove();
                        if(settings.successIcon && $vobj["showIcon"]) $el.parents(".input").append('<span class="icsuccess"></span>');
                    }
                }
            }
            if(settings.message !== "inline"){
                $alertModule.find('h4').html(settings.title);
                $alertModule.show();
            }
            if(errorList.length < 1){
                utils._reset($parent);
                $alertModule.hide();
                return true;
            }
            return false;            
        },
        _getLabel: function($field){
            var $label = "This field";
            if($field.prev("label").length)
                $label = $field.prev("label");
            else if($field.parents(".input").prev().find("label").length)
                $label = $field.parents(".input").prev().find("label");
            
            return (typeof $label === "object" ? ($label.contents().length > 1) ? $.trim($label.contents()[1].data) : $label.text() : $label);
        },
        _minmax: function($labelText){
            var $field = $(this);
            if($field.attr("min") || $field.attr("max")){
                if($field.attr("min") && $.validator.min($field))
                    utils._setError.call(this, true, $labelText+' should not less than '+$field.attr("min")+' characters', true, false);
                else if($field.attr("max") && $.validator.max($field))
                    utils._setError.call(this, true, $labelText+' should not more than '+$field.attr("max")+' characters', true, false);
                else
                    utils._setError.call(this, false, "", false, true);
            }
        },
        _validate: function($arr){
            var self = this;
            $.each($arr, function(i, val){
                val = $.trim(val);
                if (val in $.validator.rules) {
                    //Below code will apply rules for each field
                    if(!$.validator.rules[val]($field)){
                        //Below code will retrieve value of data-validation-msg and display based on errors. 
                        if($field.data('validation-msg')){
                            var $errormsgs = $field.data('validation-msg');
                            $messages = ($errormsgs.indexOf("|")) ? $errormsgs.split("|") : $errormsgs;
                            $message = ($.trim($messages[i])) ? $.trim($messages[i]) : $labelText+" needs valid input";
                        }
                        else
                            $message = $labelText+" needs valid input"; //Default error message when data-validation-msg attribute is not found.
                    }
                }
            });
        },
        _reset: function($parent){
            $parent.find('span.icsuccess').remove();
            $parent.find('.alertText').remove();
            $parent.find('.alertHighlight').removeClass('alertHighlight');
        },
        _setError: function($status, $message, $mstatus, $showIcon){
            this.tcvalidation = {error: $status, message: $message, showmessage: $mstatus, showIcon: $showIcon};
        }
    };

    $.fn.TIAAValidation = function(options){
        var form = this,
            $el = $(form),
            // Default options
            defaultOptions = {
                // Parent element
                parent: $el.parent(),
                // Error title
                title: "The following fields are missing",
                message: "both", // both, top, inline
                successIcon: true, // true, false
                // Below afterValidation function will run once form validation is successfully completed
                afterValidation: function(){
                    return true;
                }
            };
        $el.attr( "novalidate", "novalidate");
        // As per style guide, all passwords never be optional.
        $(form).find("input[type=password]").attr("aria-required", "true");
        // Below settings variable overrides defaultOptions (Merging user defined options with defaultOptions)
        var settings = $.extend( {}, defaultOptions, options );
        form.settings = settings;
        utils._reset($(form.settings.parent));
        // Below code will trigger once form is submitted
        $el.on("submit", function(e){
            utils._reset((typeof form.settings.parent === "string") ? $(form.settings.parent) : form.settings.parent); // Reset
            // Checks if form validation enabled in prototype settings
            
                utils._getElements($el).each(function(){
                    var self = this,
                        $field = $(self),
                        $labelText = "Field", $message;
                    //Below code will retrieve label value to display it in the error message
                    $labelText = utils._getLabel($field);
                    $message = ($field.data("required-msg")) ? $field.data("required-msg") : $labelText+" is required"
                    /*  Below code will check, whether input(including radio, checkbox) field is empty 
                        and field should not be either condition
                        Either condition: In case any one of these field is mandatory then below code will execute */
                    if(utils._isRequired($field) && $.validator.isEmpty($field) && !$field.data("either")){
                        utils._setError.call(self, true, $message, true, false);
                    }
                    else{
                        if(utils._isRequired($field)) utils._setError.call(self, false, "", false, true);

                        if($field.data("either")){
                            var $either = $($field.data("either"));
                            if($field.val()!="" || $either.val()!=""){
                                utils._setError.call(self, false, "", false, false);
                                utils._setError.call($either[0], false, "", false, false);
                                if($field.val()!=""){ 
                                    utils._minmax.call(self, $labelText);
                                    self.tcvalidation.showIcon = true;
                                }
                                if($either.val()!="") $either[0].tcvalidation.showIcon = true;
                            }
                            else{
                                utils._setError.call(self, true, $message, true, false);
                                utils._setError.call($either[0], true, $message, true, false);
                            }
                        }
                        //Below code will retrieve value of data-validation and do validation based on that. 
                        if($field.data('validation') && $field.val()!=""){
                            $validate = $field.data('validation');
                            $arr = ($validate.indexOf("|")) ? $validate.split("|") : $validate;
                            var $errCount = 0;
                            $.each($arr, function(i, val){
                                val = $.trim(val);
                                if (val in $.validator.rules) {
                                    //Below code will apply rules for each field
                                    if(!$.validator.rules[val]($field)){
                                        //Below code will retrieve value of data-validation-msg and display based on errors. 
                                        if($field.data('validation-msg')){
                                            var $errormsgs = $field.data('validation-msg');
                                            $messages = ($errormsgs.indexOf("|")) ? $errormsgs.split("|") : $errormsgs;
                                            $message = ($.trim($messages[i])) ? $.trim($messages[i]) : $labelText+" needs valid input";
                                        }
                                        else
                                            $message = $labelText+" needs valid input"; //Default error message when data-validation-msg attribute is not found.
                                        utils._setError.call(self, true, $message, true, false);
                                        $errCount++;
                                    }
                                }
                            });
                            if(!$errCount){
                                utils._setError.call(self, false, "", false, true);
                                utils._minmax.call(self, $labelText);
                            } 
                        }
                        else
                            if(!utils._isRequired($field) && $field.data('validation')) utils._setError.call(self, false, "", false, false);
                    }
                });
                if(utils._errorHandler.call(form) === false) return false;
            
            //Below code will run when all the fields in the form are validated.
            if(typeof form.settings.afterValidation === "function") return form.settings.afterValidation();
        });
    };


    //Validation rules
    $.validator = {
        isEmpty : function($el){
            return (this.getLength($el) <= 0);
        },
        rules: {
            isString : function($el){
                return (/^[a-zA-Z ]+$/.test($el.val()));
            },
            isAlphaNumeric : function($el){
                return (/^[a-zA-Z0-9 ]+$/.test($el.val()));
            },
            isNumber : function($el){
                return (/^[0-9]+$/.test($el.val()));
            },
            isSame : function($el){
                $another = $($el.data("target"));
                return ($el.val() === $another.val());
            },
            isDate: function($el){
                if(!/^\d{2}\/\d{2}\/\d{4}$/.test($el.val())) return false;
                if($el.val().indexOf("/")){
                    var parts = $el.val().split("/"),
                    month = parseInt(parts[0], 10),
                    day = parseInt(parts[1], 10),
                    year = parseInt(parts[2], 10),
                    monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
                    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                        monthLength[1] = 29;                        
                    if(year < 1000 || year > 3000 || month == 0 || month > 12)
                        return false;
                    else if(!(day > 0 && day <= monthLength[month - 1]))
                        return false;
                    else
                        return true;
                }
                else
                    return false;
            },
            isEmail: function($el){
                return (/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.([A-Za-z0-9]{2,4})+$/.test($el.val()));
            },
            isUrl: function($el){
                return (/(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/.test($el.val()));
            }
        },
        min: function($el){
            return (parseInt($el.attr("min")) > this.getLength($el));
        },
        max: function($el){
            return (parseInt($el.attr("max")) < this.getLength($el));
        },
        getLength: function($el){
            switch($el[0].nodeName.toLowerCase()) {
            case "select":
                    var a = $el.find("option:selected:not(:disabled)").val(),
                        len = (a=== undefined || a=== "") ? 0 : 1;
                return len;
            case "input":
                if ( this.isCheckable( $el) ) {
                    return ($el.parents(".input, .lblSelectPair").find("[name="+$el.attr("name")+"]:checked").length);
                }
            }
            return $.trim($el.val()).length;
        },
        isCheckable : function(b){
            return (b[0].type.toLowerCase() === "checkbox" || b[0].type.toLowerCase() === "radio") ? true : false;
        },
        addRule: function(name, rule){
            if(typeof name !== "string")
                throw new TypeError("Rule name should be in 'string'");
            else{
                if(typeof rule !== "function") throw new TypeError("Rule should be as 'function'");
                else $.validator.rules[name] = rule;
            }
        }
    };

})();
